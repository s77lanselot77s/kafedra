<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 13.04.2016
 * Time: 16:19
 */

namespace app\widgets;

use app\models\JournalRecords;
use yii\base\Widget;
use app\models\Timetable;
use yii\grid\GridView;
use yii\helpers\Html;

class JournalWidget extends Widget
{
    public $students;

    public $journalRecords;

    public $journal;

    public $group;

    private $obj;
    private $year;
    private $sem;
    private $months;
    private $data = array();

    public function init()
    {
        $this->obj = new JournalRecords();
        foreach ($this->journalRecords as $journalRecord) {
            $dateTime = new \DateTime($journalRecord->date);
            $this->data[$journalRecord->student_id][$dateTime->format('n')][$dateTime->format('d')] = ['value' => $journalRecord->value, 'record' => $journalRecord->id];
        }
        $this->year = $this->journal->tsg->year;
        $this->sem = $this->journal->tsg->sem;
        if($this->sem == 1)
            $this->months = [9, 10, 11, 12 , 1];
        elseif($this->sem == 2)
            $this->months = [2, 3, 4, 5 , 6];
        //var_dump($this->data['3']);die;
    }

    public function run()
    {
        $result = $attributes = '';
        $result .= Html::beginTag('div', ['class' => 'table-responsive']);
        $result .= Html::beginTag('table', $options = ['class' => 'journal-values table table-striped table-bordered', 'style' => 'font-size: 10px;']);
            $result .= Html::beginTag('thead');
            $result .= $this->getDatesHeaders();
            $result .= Html::endTag('thead');
            $result .= Html::beginTag('tbody');
            $result .= Html::beginTag('tr');

            $result .= $this->getValuesByStudent();

            $result .= Html::endTag('tr');

        $result .= Html::endTag('tbody');
        $result .= Html::endTag('table');
        $result .= Html::endTag('div');

        return $this->getTableStudents() . $result;

    }

    private function getDatesHeaders()
    {
        $result = $monthRes = $daysRes = '';

        $monthRes .= Html::beginTag('tr');
        $daysRes .= Html::beginTag('tr');
        foreach ($this->months as $m) {
            $monthRes .= Html::tag('th', $this->getMonthLabels($m), ['colspan' => cal_days_in_month(CAL_GREGORIAN, $m, $this->year), 'class' => 'month-label']);
            for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, $m, $this->year); $i++) {
                $daysRes .= Html::tag('th', $i);
            }
        }
        //var_dump($result);die;
        $monthRes .= Html::endTag('tr');
        $daysRes .= Html::endTag('tr');
        //$result .= Html::beginTag('tr') . Html::tag('th', 'студент', ['rowspan' => 3, 'style' => 'vertical-align: middle; text-align: center']) . Html::endTag('tr');
        $result .= $monthRes . $daysRes;
        return $result;

    }

    private function getValuesByStudent()
    {
        $result = '';
        foreach ($this->students as $student) {
            $result .= Html::beginTag('tr', ['student' => $student->user_id]);
            //$result .= Html::tag('td', $student->shortFio);
            foreach ($this->months as $m) {
                //$result .= Html::tag('th', $this->getMonthLabels($m), ['colspan' => cal_days_in_month(CAL_GREGORIAN, $m, $this->year), 'class' => 'month-label']);
                for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, $m, $this->year); $i++) {
                    if(isset($this->data[$student->user_id]) && isset($this->data[$student->user_id][$m]) && isset($this->data[$student->user_id][$m][$i])) {
                        $result .= Html::tag('td', $this->data[$student->user_id][$m][$i]['value'], ['class' => 'value', 'student' => $student->user_id, 'group' => $this->group, 'year' => $this->year, 'month' => $m, 'day' => $i, 'journal' => $this->journal->id, 'record' => $this->data[$student->user_id][$m][$i]['record']]);
                    }
                     else $result .= Html::tag('td', '', ['class' => 'value', 'student' => $student->user_id, 'group' => $this->group, 'year' => $this->year, 'month' => $m, 'day' => $i, 'journal' => $this->journal->id, 'record' => 'NaN']);
                }
            }
            $result .= Html::endTag('tr');
        }

        //var_dump($result);d
        return $result;

    }

    private function getTableStudents()
    {
        $result = '';
        $result .= Html::beginTag('div', ['class' => 'students']);
        $result .= Html::beginTag('table', ['class' => 'journal-students table table-striped table-bordered']);
        $result .= Html::beginTag('thead');
        $result .= Html::beginTag('tr');
        $result .= Html::tag('th', 'студент', ['style' => 'vertical-align: middle; text-align: center']);
        $result .= Html::endTag('tr');
        $result .= Html::endTag('thead');
        $result .= Html::beginTag('tbody');
        foreach ($this->students as $student) {
            $result .= Html::beginTag('tr');
            $result .= Html::tag('td', $student->shortFio);
            $result .= Html::endTag('tr');
        }
        $result .= Html::endTag('tbody');
        $result .= Html::endTag('table');
        $result .= Html::endTag('div');
        return $result;
    }

    private function getMonthLabels($month)
    {
        $months = [
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11=> 'Ноябрь',
            12=> 'Декабрь',
        ];

        return $months[$month];
    }


}
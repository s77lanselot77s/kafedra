<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 13.04.2016
 * Time: 16:19
 */

namespace app\widgets;

use yii\base\Widget;
use app\models\Timetable;
use yii\grid\GridView;
use yii\helpers\Html;

class TimetableWidget extends Widget
{
    public $group;

    private $timetable;
    private $obj;

    public function init()
    {
        $this->timetable = Timetable::find()->joinWith(['teacher'])->where('groups_id = :group', [':group' => $this->group])->orderBy(['time' => SORT_ASC])->all();
        $this->obj = new Timetable();
    }

    public function run()
    {
        $result = $attributes = '';
        $elements = $this->getElements();
        $timeT = new Timetable();
        $result .= Html::beginTag('div', $options = ['class' => 'table-responsive']);
        $result .= Html::beginTag('table', $options = ['class' => 'table table-striped table-bordered', 'style' => 'font-size: 10px;']);
            $result .= Html::beginTag('thead');
            $result .= Html::beginTag('tr');
            $attributes .= Html::beginTag('tr');
            foreach ($timeT->days as $key => $value) {
                $result .= Html::tag('th', $value, ['colspan' => 6 ,'style' => 'text-align: center; border-right: 3px solid #ddd;']);
                $attributes .= Html::tag('th', 'Время');
                $attributes .= Html::tag('th', 'Предмет');
                $attributes .= Html::tag('th', 'Тип');
                $attributes .= Html::tag('th', 'Кабинет');
                $attributes .= Html::tag('th', 'Преподаватель');
                $attributes .= Html::tag('th', 'Неделя', ['style' => 'border-right: 3px solid #ddd;']);
            }
            $result .= Html::endTag('tr');
            $attributes .= Html::endTag('tr');
            $result .= $attributes;

            $result .= Html::endTag('thead');
            $result .= Html::beginTag('tbody');
            for ($i = 0; $i < 10; $i++) {
                $result .= Html::beginTag('tr');
                    foreach($timeT->days as $key => $val) {
                       if(isset($elements[$key][$i]['dmt']) AND $elements[$key][$i]['dmt']) {
                           $result .= Html::tag('td', 'День военной подготовки', ['colspan' => 6, 'style' => 'text-align: center; vertical-align: middle']);
                           continue;
                       } else
                       $result .= Html::tag('td', $elements[$key][$i]['time']);
                       $result .= Html::tag('td', $elements[$key][$i]['title']);
                       $result .= Html::tag('td', $elements[$key][$i]['type']);
                       $result .= Html::tag('td', $elements[$key][$i]['room']);
                       $result .= Html::tag('td', $elements[$key][$i]['author']);
                       $result .= Html::tag('td', $elements[$key][$i]['week'], ['style' => 'border-right: 3px solid #ddd;']);
                }
                $result .= Html::endTag('tr');
            }
        $result .= Html::endTag('tbody');
        $result .= Html::endTag('table');
        $result .= Html::endTag('div');

        return $result;

    }

    private function getElements()
    {
        $result = array();
        foreach ($this->timetable as $item) {
            $week = ($item->week != 0) ? '  ' . $this->obj->weeks[$item->week] : '';
            $time = new \DateTime($item->time);

            $result[$item->day][] = [
                'time' => $time->format('H:i'),
                'title' => $item->title,
                'type' => $this->obj->types[$item->type],
                'room' => $item->room,
                'author' => $item->teacher->shortFio ,
                'week' => $week,
                'dmt' => $item->dmt,
            ];
        }

        return $result;
    }


}
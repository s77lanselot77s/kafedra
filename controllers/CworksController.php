<?php

namespace app\controllers;

use app\models\CworksDefault;
use app\models\Groups;
use app\models\Students;
use app\models\TeacherSubjectGroups;
use Yii;
use app\models\Cworks;
use app\models\search\CworksSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use DateTime;
/**
 * CworksController implements the CRUD actions for Cworks model.
 */
class CworksController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'delete', 'view', 'export-word', 'update-default-list'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reject', 'zav-kaf-confirm'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['admin', 'student'],
                    ],
                    [
                        'actions' => ['reject', 'teacher-confirm'],
                        'allow' => true,
                        'roles' => ['teacher'],
                    ],
                    [
                        'actions' => ['send-for-confirm'],
                        'allow' => true,
                        'roles' => ['student'],
                    ],
                ]
            ]
        ];
    }

    /**
     * Lists all Cworks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CworksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cworks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cworks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cworks();
        if($model->load(Yii::$app->request->post()))
        {
            if(Yii::$app->user->can('student'))
                $model->student_id = Yii::$app->user->id;
        }
        if  ($model->save()) {
            Yii::$app->session->setFlash('success', 'Бланк на курсовое проектирование добавлен');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if (Yii::$app->user->can('student')) {
                $student = Students::findOne(['user_id'=>Yii::$app->user->id]);
                return $this->render('create', [
                    'model' => $model, 'student' => $student,
                ]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionExportWord($id)
    {
        $id = (int)$id;
        //$model = new Cworks();
        $model = Cworks::findOne(['id'=>$id]);
        if(!isset($model))
            throw new HttpException(404, 'Бланка с таким номером не существует');
        //if(Yii::$app->user->can('admin') AND $model->status != Cworks::STATUS_CONFIRM_TEACHER)
            //throw new HttpException(403, 'Заведующему кафедры не доступны для просмотра и подтверждения бланки, которые не были проверены преподавателями');
        if(Yii::$app->user->can('teacher') AND $model->status_id != Cworks::STATUS_WAITING_FOR_CONFIRM)
            throw new HttpException(403, 'Преподавателям не доступны для просмотра и подтверждения бланки, которые не были переведены в статус "' . $model->getStatus()[$model->status_id] . '"');
        if(Yii::$app->user->can('student') AND $model->status_id != Cworks::STATUS_CONFIRM_ZAVKAF)
            throw new HttpException(403, 'Студентам не доступны для загрузки бланки, которые не были подтверждены препеподавателем и заведующим кафедры');
        if(Yii::$app->user->can('teacher') AND (Yii::$app->user->id != $model->tsg->teacher_id))
            throw new HttpException(403, 'Вы попытались загрузить бланк на курсовой проект, рукодителем которого не являетесь');
        if(Yii::$app->user->can('student') AND (Yii::$app->user->id != $model->student_id))
            throw new HttpException(403, 'Вы попытались открыть чужой бланк на курсовое проектирование');
        // Initalize the TBS instance
        $OpenTBS = new \hscstudio\export\OpenTBS; // new instance of TBS
        // Change with Your template kaka
        $template =  \Yii::getAlias('@webroot').'/uploads/cworks_templates/cworks_template.docx';

        $OpenTBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

        $date = new DateTime($model->start_at);
        $start_cwork =  $date->format('d.m.y');
        $data = [
            'subject'=>$model->tsg->subjectBranch->subject->title,
            'stud_shortFio'=>$model->student->shortFio,
            'group'=>$model->student->group->title,
            'ptitle'=>$model->ptitle,
            'source_data'=>$model->source_data,
            'calculation_part'=>$model->calculation_part,
            'graphic_part'=>$model->graphic_part,
            'exper_part'=>$model->exper_part,
            'start_date'=>$start_cwork,
            'teacher_shortFio'=>$model->tsg->teacher->shortFio,
            'year'=>date('Y'),
        ];
        //var_dump($data['subject']);die;
        $OpenTBS->MergeField('data', $data);
        // Output the result as a file on the server. You can change output file
        $OpenTBS->Show(OPENTBS_DOWNLOAD, 'export.docx');
        exit;
    }

    /*public function actionAddBlank()
    {

    }*/

    public function actionSendForConfirm($id)
    {
        $id = (int)$id;
        $cwork = Cworks::findOne(array('id' => $id));
        if(!isset($cwork))
            throw new HttpException('404', 'Бланка таким номером не всуществует');
        if($cwork->student_id != Yii::$app->user->id)
            throw new HttpException('403', 'У вас нет доступа к бланку с таким номером');
        if($cwork->status_id != Cworks::STATUS_ADDED or $cwork->status_id == Cworks::STATUS_REJECTED)
            throw new HttpException('500', 'Данный бланк уже проверяется или был отклонен. Исправьте ошибки и снова отправьте его на проверку');
        $cwork->status_id = Cworks::STATUS_WAITING_FOR_CONFIRM;
        $cwork->update();
        Yii::$app->session->setFlash('success', 'Статус бланка был изменен. Бланк ожидает проверки преподавателем');
        $this->redirect('index');
    }

    public function actionTeacherConfirm($id)
    {
        $id = (int)$id;
        $cwork = Cworks::findOne(array('id' => $id));
        if(!isset($cwork))
            throw new HttpException('404', 'Бланка с таким номером не всуществует');
        if($cwork->status_id != Cworks::STATUS_WAITING_FOR_CONFIRM)
            throw new HttpException('500', 'Данный бланк не ожидает проверки преподавателем');
        if($cwork->tsg->teacher_id != Yii::$app->user->id)
            throw new HttpException('403', 'У вас нет прав на подтверждение данного бланка');
        $cwork->status_id = Cworks::STATUS_CONFIRM_TEACHER;
        $cwork->update();
        Yii::$app->session->setFlash('success', 'Статус бланка был изменен. Бланк ожидает проверки заведующим кафедрой');
        $this->redirect('index');
    }

    public function actionZavKafConfirm($id)
    {
        $id = (int)$id;
        $cwork = Cworks::findOne(array('id' => $id));
        if(!isset($cwork))
            throw new HttpException('404', 'Бланка с таким номером не всуществует');
        if($cwork->status_id != Cworks::STATUS_CONFIRM_TEACHER)
            throw new HttpException('500', 'Данный бланк не ожидает проверки заведующим кафедрой');
        $cwork->status_id = Cworks::STATUS_CONFIRM_ZAVKAF;
        $cwork->update();
        Yii::$app->session->setFlash('success', 'Статус бланка был изменен. Теперь студент сможет загрузить подтвержденный бланк');
        $this->redirect('index');
    }

    public function actionReject($id)
    {
        $id = (int)$id;
        $cwork = Cworks::findOne(array('id' => $id));
        if(!isset($cwork))
            throw new HttpException('404', 'Бланка с таким номером не всуществует');
        if($cwork->status_id == Cworks::STATUS_ADDED or $cwork->status_id == Cworks::STATUS_CONFIRM_ZAVKAF)
            throw new HttpException('500', 'Данный бланк не может быть отклонен, так как он не ожидает чьей либо проверки, либо уже был подтвержден заведующим кафедрой');
        if(Yii::$app->user->can('teacher') and $cwork->tsg->teacher_id != Yii::$app->user->id)
            throw new HttpException('403', 'у вас недостаточно прав для того, чтобы отклонить данный бланк');
        $cwork->status_id = Cworks::STATUS_REJECTED;
        $cwork->update();
        Yii::$app->session->setFlash('success', 'Статус бланка был изменен. Бланк был отклонен');
        $this->redirect('/cworks/index');

    }

    public function actionDownload($id)
    {
        /*...*/
    }

    public function actionUpdateDefaultList()
    {
        $id = (int)$_POST['id'];
        $countPosts = CworksDefault::find()
            ->where(['tsg_id' => $id])
            ->count();

        $posts = CworksDefault::find()
            ->where(['tsg_id' => $id])
            ->orderBy('id DESC')
            ->all();

        if($countPosts>0){
            foreach($posts as $post){
                echo json_encode("<option value='".$post->id."'>".$post->ptitle."</option>");
            }
        }
        else{
            echo json_encode("<option>Шаблоны отсутствуют</option>");
        }

    }
    /**
     * Updates an existing Cworks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->status_id = 0;
            $model->save();
            Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Cworks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cworks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cworks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cworks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

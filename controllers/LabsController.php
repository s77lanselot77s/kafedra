<?php

namespace app\controllers;

use app\models\TeacherSubjectGroups;
use Yii;
use app\models\Labs;
use app\models\search\LabsSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LabsController implements the CRUD actions for Labs model.
 */
class LabsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Labs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LabsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Labs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Labs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Labs();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Работа добавлена');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateGroups()
    {
        if(isset($_POST['id'])) {
            $id = (int)$_POST['id'];
            $tsg = TeacherSubjectGroups::findOne(['id' => $id]);
            $result = '<option value="">Выберите группу</option>';
            if(isset($tsg))
            {
                $groupsArray = ArrayHelper::map($tsg->subjectBranch->branch->groups, 'id', 'title');
                foreach ($groupsArray as $key => $value) {
                    $result .= '<option value="' . $key . '">' . $value . '</option>';
                }
            }
            return $result;
        }
        throw new \yii\web\HttpException(500, 'Not set id');

    }

    /**
     * Updates an existing Labs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Работа изменена');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Labs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDownload($filename)
    {
        $labs = Labs::find()->where('file = :file', [':file' => $filename])->one();

        if(!isset($labs))
            throw new HttpException(404, 'Файл не найден');
        Yii::$app->response->sendFile(Yii::getAlias('@webroot') . '/uploads/labs/' . $filename);
    }

    /**
     * Finds the Labs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Labs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Labs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use Yii;
use app\models\JournalRecords;
use app\models\search\JournalRecordsSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JournalRecordsController implements the CRUD actions for JournalRecords model.
 */
class JournalRecordsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all JournalRecords models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JournalRecordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JournalRecords model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JournalRecords model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JournalRecords();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing JournalRecords model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {

            if($_POST['record'] == 'NaN') {
                $jr = new JournalRecords();
                $jr->journal_id = $_POST['journal'];
                $jr->group_id = $_POST['group'];
                $jr->student_id = $_POST['student'];
                $jr->value = $_POST['value'];
                $jr->date = $_POST['year'] . '-' . str_pad($_POST['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($_POST['day'], 2, '0', STR_PAD_LEFT);
                if(!$jr->save())
                    var_dump($jr->getErrors());die;
            } else {
                $jr = JournalRecords::findOne(['id' => (int)$_POST['record']]);
                if(!isset($jr))
                    throw new HttpException(404, 'Запись не найдена');
                if($jr->group_id != $_POST['group'])
                    throw new HttpException(404, 'Группа не совпадает');
                if($jr->journal_id != $_POST['journal'])
                    throw new HttpException(404, 'Журнал не совпадает');
                if($jr->student_id != $_POST['student'])
                    throw new HttpException('id студента не совпадает');
                $jr->value = $_POST['value'];
                $jr->date = $_POST['year'] . '-' . str_pad($_POST['month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($_POST['day'], 2, '0', STR_PAD_LEFT);
                if($jr->value == '')
                    $jr->delete();
                else
                    $jr->update();

            }

            return true;

    }

    /**
     * Deletes an existing JournalRecords model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JournalRecords model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JournalRecords the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JournalRecords::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\controllers\user;

use app\models\Students;
use app\models\Teachers;
use Yii;
use yii\web\Controller;
use dektrium\user\Finder;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use dektrium\user\controllers\ProfileController as BaseProfileController;

/**
 * ProfileController shows users profiles.
 *
 * @property \dektrium\user\Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class ProfileController extends BaseProfileController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['index'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['show'], 'roles' => ['?', '@']],
                ]
            ],
        ];
    }

    /**
     * Redirects to current user's profile.
     * @return \yii\web\Response
     */
    public function actionIndex()
    {
        return $this->redirect(['show', 'id' => \Yii::$app->user->getId()]);
    }

    /**
     * Shows user's profile.
     * @param  integer $id
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionShow($id)
    {
        if(Yii::$app->user->can("admin")) {
            $profile = $this->finder->findProfileById($id);
            $view = 'show';
        }
        if(Yii::$app->user->can("teacher")) {
            $profile = Teachers::findOne(array('user_id' => \Yii::$app->user->id));
            $view = 'teacher';
        }
        if(Yii::$app->user->can("student")) {
            $profile = Students::findOne(array('user_id' => \Yii::$app->user->id));
            $view = 'student';
        }
        if ($profile === null) {
            throw new NotFoundHttpException;
        }
        return $this->render($view, [
            'profile' => $profile,
        ]);
    }
}

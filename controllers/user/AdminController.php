<?php

namespace app\controllers\user;

use app\models\Students;
use dektrium\user\helpers\Password;
use Yii;
use app\models\Teachers;
use app\models\user\User;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use dektrium\user\controllers\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'  => ['post'],
                    'confirm' => ['post'],
                    'block'   => ['post']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'delete', 'block', 'confirm'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->identity->getIsAdmin();
                        }
                    ],
                    [
                        'actions' => ['create-teacher'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['create-student'],
                        'allow' => true,
                        'roles' => ['admin', 'teacher'],
                    ],
                ]
            ]
        ];
    }
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $user->scenario = 'update';
        $profile = $this->finder->findProfileById($id);
        $r = \Yii::$app->request;

        $this->performAjaxValidation([$user, $profile]);

        if ($user->load($r->post()) && $profile->load($r->post()) && $user->save() && $profile->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('user', 'User has been updated'));
            return $this->refresh();
        }

        return $this->render('update', [
            'user'    => $user,
            'profile' => $profile,
            'module'  => $this->module,
        ]);
    }

    public function actionCreateTeacher()
    {
        $user = new User();
        $user->scenario = 'create';
        $teacherProfile = new Teachers();
        $this->performAjaxValidation([$user, $teacherProfile]);
        if ($user->load(Yii::$app->request->post()) && $teacherProfile->load(Yii::$app->request->post()))
        {
            if($user->validate() && $teacherProfile->validate())
            {
                if($user->password == null )
                    $user->password = Password::generate(8);
                $user->save(false);
                $teacherProfile->user_id = $user->id;
                $teacherProfile->save(false);
                $userRole = Yii::$app->authManager->getRole('teacher');
                Yii::$app->authManager->assign($userRole, $user->getId());
                Yii::$app->mailer->compose()
                    ->setFrom('admin@kafedra.dev')
                    ->setTo($user->email)
                    ->setSubject('Данные для авторизации на сайте кафедры ВТ')
                    ->setTextBody('Здравствуйте ' . $teacherProfile->fname . ' ' . $teacherProfile->lname .
                        '!. Вы были зарегистрированы на сайте кафедры "Вычислительная Техника" в качестве преподавателя. Ваши данные для авторизации:\n <b>Логин:</b> '
                        . $user->username . '\n<b>Пароль:</b>' . $user->password)
                    ->send();
                Yii::$app->session->setFlash('success','Аккаунт преподавателя успешно создан');
                return $this->refresh();
            }
        }
        return $this->render('create', ['user'=>$user, 'teacherProfile'=>$teacherProfile]);
    }

    public function actionCreateStudent()
    {
        $user = new User();
        $user->scenario = 'create';
        $studentProfile = new Students();
        $this->performAjaxValidation([$user, $studentProfile]);
        if ($user->load(Yii::$app->request->post()) && $studentProfile->load(Yii::$app->request->post()))
        {
            if($user->validate() && $studentProfile->validate())
            {
                if($user->password == null )
                    $user->password = Password::generate(8);
                $user->save(false);
                $studentProfile->user_id = $user->id;
                $studentProfile->save(false);
                $userRole = Yii::$app->authManager->getRole('student');
                Yii::$app->authManager->assign($userRole, $user->getId());
                Yii::$app->mailer->compose()
                    ->setFrom('admin@kafedra.dev')
                    ->setTo($user->email)
                    ->setSubject('Данные для авторизации на сайте кафедры ВТ')
                    ->setHtmlBody('Здравствуйте ' . $studentProfile->fname .
                        '! Вы были зарегистрированы на сайте кафедры "Вычислительная Техника" в качестве студента.</br>Ваши данные для авторизации:</br><b>Логин:</b> '
                        . $user->username . '</br><b>Пароль: </b>' . $user->password)
                    ->send();
                Yii::$app->session->setFlash('success','Аккаунт студента успешно создан');
                return $this->refresh();
            }

        }
        return $this->render('create', ['user'=>$user, 'studentProfile'=>$studentProfile]);
    }
}
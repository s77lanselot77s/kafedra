<?php

namespace app\controllers\user;

use app\models\Students;
use app\models\Teachers;
use dektrium\user\controllers\RegistrationController as BaseRegistrationController;
use yii\filters\AccessControl;

class RegistrationController extends BaseRegistrationController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['register', 'connect'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['confirm', 'resend'], 'roles' => ['?', '@']],
                ]
            ],
        ];
    }


    public function actionStudent()
    {
        $student = \Yii::createObject(['class'=>Students::className(), 'scenario'=>'create']);
    }

    public function actionTeacher()
    {
        $teacher = \Yii::createObject(['class'=>Teachers::className(), 'scenario'=>'create']);

    }
}
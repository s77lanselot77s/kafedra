<?php

namespace app\controllers;

use app\models\TeacherSubjectGroups;
use Yii;
use app\models\LabsStudents;
use app\models\search\LabsStudentsSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LabsStudentsController implements the CRUD actions for LabsStudents model.
 */
class LabsStudentsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LabsStudents models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LabsStudentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(Yii::$app->user->can('student'))
            $dataProvider->query->andWhere('student_id = :student', [':student' => Yii::$app->user->id]);
        if(Yii::$app->user->can('teacher'))
            $dataProvider->query->andWhere('teacher_subject_branch.id = labs.tsg_id AND teacher_subject_branch.teacher_id = :teacher', [':teacher' => Yii::$app->user->id]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LabsStudents model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LabsStudents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LabsStudents();
        if(!Yii::$app->user->can('student'))
            throw new HttpException(403, 'Только студенты могут отправлять лабораторные работы на проверку');

        if ($model->load(Yii::$app->request->post())) {
            $model->student_id = Yii::$app->user->id;
            $model->points = 0;
            if($model->save()){
                Yii::$app->session->setFlash('success', 'Работа отправлена');
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LabsStudents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('update');
        if(Yii::$app->user->can('student') AND $model->student_id != Yii::$app->user->id) {
            throw new HttpException(403, 'Нельзя редактировать чужие работы');
        }
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->user->can('student')) {
                $model->points = $model->getOldAttribute('points');
            }
            if (Yii::$app->user->can('teacher')) {
                $tsg = TeacherSubjectGroups::find()->innerJoinWith(['subjectBranches', 'subjectBranches.branch', 'subjectBranches.branch.groups'])->where('teacher_subject_branch.id = :tsg AND teacher_subject_branch.teacher_id = :teacher AND groups.id = :group', [':teacher' => Yii::$app->user->id, ':group' => $model->labs->group_id, ':tsg' => $model->labs->tsg_id]);
            }
            if ((Yii::$app->user->can('student') AND Yii::$app->user->id == $model->student_id) OR (Yii::$app->user->can('teacher') AND isset($tsg))) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Изменения сохранены');
                    $model->sendNotification();
                    return $this->render('view', ['id' => $model->id]);
                }
            } else
                throw new HttpException(403, 'У вас нет доступа к данной работе');
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDownload($filename)
    {
        $labs = LabsStudents::find()->where('file = :file', [':file' => $filename])->one();

        if(!isset($labs))
            throw new HttpException(404, 'Файл не найден');
        if(Yii::$app->user->can('teacher')) {
            $tsg = TeacherSubjectGroups::find()->innerJoinWith(['subjectBranches', 'subjectBranches.branch', 'subjectBranches.branch.groups'])->where('teacher_subject_branch.id = :tsg AND teacher_subject_branch.teacher_id = :teacher AND groups.id = :group', [':teacher' => Yii::$app->user->id, ':group' => $labs->labs->group_id, ':tsg' => $labs->labs->tsg_id]);
        }
        if((Yii::$app->user->can('student') AND Yii::$app->user->id == $labs->student_id) OR (Yii::$app->user->can('teacher') AND isset($tsg)))
            Yii::$app->response->sendFile(Yii::getAlias('@webroot') . '/uploads/labs_students/' . $filename);
        else
            throw new HttpException(403, 'У вас нет доступа к данной работе');
    }

    /**
     * Deletes an existing LabsStudents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if(Yii::$app->user->can('teacher')) {
            $tsg = TeacherSubjectGroups::find()->innerJoinWith(['subjectBranches', 'subjectBranches.branch', 'subjectBranches.branch.groups'])->where('teacher_subject_branch.id = :tsg AND teacher_subject_branch.teacher_id = :teacher AND groups.id = :group', [':teacher' => Yii::$app->user->id, ':group' => $model->labs->group_id, ':tsg' => $model->labs->tsg_id]);
            if(!isset($tsg))
                throw new \HttpException(403, 'У вас нет доступа к данной работе');
        }
        if($model->delete())
            Yii::$app->session->setFlash('success', 'Работа удалена');

        return $this->redirect(['index']);
    }

    /**
     * Finds the LabsStudents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LabsStudents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LabsStudents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use app\models\Groups;
use app\models\JournalRecords;
use app\models\Students;
use app\models\TeacherSubjectGroups;
use Yii;
use app\models\Journal;
use app\models\search\JournalSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;


/**
 * JournalController implements the CRUD actions for Journal model.
 */
class JournalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Journal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JournalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Journal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $group)
    {
        $journal = Journal::find()->where('id = :id', [':id' => $id])->one();
        if(!isset($journal))
            throw new HttpException(404, 'Журнал не найден');
        $groupModel = Groups::find()->joinWith(['branch', 'branch.subjectBranches'])->where('groups.id = :group', [':group' => $group])->one();
        if(!isset($groupModel))
            throw new HttpException(404, 'Нет такой группы');
        $tsgModel = TeacherSubjectGroups::find()->joinWith(['subjectBranches', 'subjectBranches.branch', 'subjectBranches.branch.groups'])->where('teacher_subject_branch.id = :tsg AND teacher_subject_branch.teacher_id = :teacher AND groups.id = :group', [':teacher' => Yii::$app->user->id, ':group' => $group, ':tsg' => $journal->tsg_id]);
        if(!isset($tsgModel))
            throw new HttpException(403, 'У вас недостаточно прав для доступа к этому журналу');
        $journalRecords = JournalRecords::find()->joinWith(['journal', 'journal.tsg'])->where('journal_id = :journal AND group_id = :group', [':journal' => $journal->id, ':group' => $group])->orderBy('date ASC')->all();
        $students = Students::find()->where('group_id = :group', [':group' => $group])->orderBy('lname')->all();
        /*$dataProvider = new ActiveDataProvider([
            'query' => JournalRecords::find()->where('journal_id = :journal AND group_id = :group', [':journal' => $journal->id, ':group' => $group])->orderBy('date ASC'),
        ]);*/
        return $this->render('view', [
            'model' => $journal,
            'students' => $students,
            'records' => $journalRecords,
            'group' => $groupModel,
            //'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Journal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Journal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Journal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Journal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Journal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Journal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Journal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

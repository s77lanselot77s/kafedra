<?php

namespace app\controllers;

use app\models\Groups;
use Yii;
use app\models\Messages;
use app\models\search\MessagesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
/**
 * MessagesController implements the CRUD actions for Messages model.
 */
class MessagesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Messages models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*$searchModel = new MessagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/

        $dataProviderAll = new ActiveDataProvider([
            'query' => Messages::find()->where('(user_from = :user OR user_to = :user) AND user_id = :user', [':user' => Yii::$app->user->id])->orderBy('messages.id DESC'),
        ]);

        $dataProviderInbox = new ActiveDataProvider([
            'query' => Messages::find()->where('user_to = :user AND user_id = :user', [':user' => Yii::$app->user->id])->orderBy('messages.id DESC'),
        ]);

        /*foreach ($dataProviderInbox->models as $model) {
            if ($model->status == 0) {
                $model->status = 1;
                $model->update();
            }
        }*/

        $dataProviderOutbox = new ActiveDataProvider([
            'query' => Messages::find()->where('user_from = :user AND user_id = :user', [':user' => Yii::$app->user->id])->orderBy('messages.id DESC'),
        ]);


        return $this->render('index', ['dataProviderAll' => $dataProviderAll, 'dataProviderInbox' => $dataProviderInbox, 'dataProviderOutbox' => $dataProviderOutbox]);

    }

    /**
     * Displays a single Messages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Messages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Messages(['scenario' => 'create']);


        if ($model->load(Yii::$app->request->post())) {
            if(isset($_FILES['Messages']['name']['attachFile']));
                $fileName = Yii::$app->security->generateRandomString();
            if(is_array($model->teachersTo))
            {
                $model->saveMessage($model->teachersTo, $fileName);
            }
            if(is_array($model->groupsTo))
            {
                foreach ($model->groupsTo as $groups) {
                    $group = Groups::findOne(['id' => $groups]);
                    if(isset($group) AND isset($groups->students))
                        $model->saveMessage($groups->students, $fileName);
                }
            }
            if(is_array($model->studentsTo))
            {
                $model->saveMessage($model->studentsTo, $fileName);
            }
            Yii::$app->session->setFlash('success', 'Сообщение успешно отправлено');
                return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionDownload($filename)
    {
        $message = Messages::find()->where('file = :file AND (user_from = :user OR user_to = :user)', [':file' => $filename, ':user' => Yii::$app->user->id])->one();
        if(!isset($message))
            throw new HttpException(404, 'Файл не найден');
        Yii::$app->response->sendFile(Yii::getAlias('@webroot') . '/uploads/personal_message/' . $filename);
    }

    /**
     * Updates an existing Messages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Messages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Messages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Messages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Messages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

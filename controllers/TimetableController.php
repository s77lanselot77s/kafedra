<?php

namespace app\controllers;

use app\models\Groups;
use Yii;
use app\models\Timetable;
use app\models\search\TimetableSearch;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TimetableController implements the CRUD actions for Timetable model.
 */
class TimetableController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Timetable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Groups::find()
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Timetable model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $group = Groups::findOne(['id' => $id]);
        if(!isset($group))
            throw new HttpException(404, 'Нет такой группы');

        return $this->render('view', [
            'group' => $group,
        ]);
    }

    /**
     * Creates a new Timetable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($group)
    {


        $model = new Timetable();
        $model->week = 2;
        $this->performAjaxValidation($model);

        $groupModel = Groups::findOne(['id' => $group]);
        if(!isset($groupModel))
            throw new HttpException(404, 'Группа не найдена');

        $columns = Timetable::findAll(['groups_id' => $group]);
        $items = array();
        $items['0'] = $items['1'] = $items['2'] = $items['3'] = $items['4'] = $items['5'] = array();
        $iterator = 0;
        foreach ($columns as $column) {
            if($column->dmt) {
               // $items[$column['day']] = array();
                $items[$column['day']] = null;
                $items[$column['day']][$iterator]['dmt'] = $column->dmt;
                //var_dump(array_values($items[$column['day']]));die;
                continue;
            }
            $items[$column['day']][$iterator]['id'] = $column->id;
            $items[$column['day']][$iterator]['week'] = $column->week;
            $items[$column['day']][$iterator]['teacher_id'] = $column->teacher_id;
            $items[$column['day']][$iterator]['title'] = $column->title;
            $items[$column['day']][$iterator]['type'] = $column->type;
            $items[$column['day']][$iterator]['room'] = $column->room;
            $items[$column['day']][$iterator]['time'] = $column->time;
            //$items[$column['day']][$iterator]['dmt'] = $column->dmt;
            $iterator++;
        }

        $model->timetable0 = array_values($items['0']);
        $model->timetable1 = array_values($items['1']);
        $model->timetable2 = array_values($items['2']);
        $model->timetable3 = array_values($items['3']);
        $model->timetable4 = array_values($items['4']);
        $model->timetable5 = array_values($items['5']);

        if ($model->load(Yii::$app->request->post())) {

            /*var_dump($model->timetable0);
            var_dump($model->timetable1);
            var_dump($model->timetable2);
            var_dump($model->timetable3);
            var_dump($model->timetable4);
            var_dump($model->timetable5);die;*/
            Timetable::updateByDay($model->timetable0, 0, $group);
            Timetable::updateByDay($model->timetable1, 1, $group);
            Timetable::updateByDay($model->timetable2, 2, $group);
            Timetable::updateByDay($model->timetable3, 3, $group);
            Timetable::updateByDay($model->timetable4, 4, $group);
            Timetable::updateByDay($model->timetable5, 5, $group);

            Yii::$app->session->setFlash('success', 'Изменения сохранены!');

            return $this->refresh();
        } else {
            return $this->render('create', [
                'model' => $model,
                'group' => $groupModel,
            ]);
        }
    }

    /**
     * Updates an existing Timetable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $columns = Timetable::findAll(['groups_id' => $id]);
        $items = array();
        $items['0'] = $items['1'] = $items['2'] = $items['3'] = $items['4'] = $items['5'] = array();
        foreach ($columns as $column) {
            $items[$column['day']]['id'] = $column->id;
            $items[$column['day']]['week'] = $column->week;
            $items[$column['day']]['teacher_id'] = $column->teacher_id;
            $items[$column['day']]['title'] = $column->title;
            $items[$column['day']]['type'] = $column->type;
            $items[$column['day']]['room'] = $column->room;
            $items[$column['day']]['time'] = $column->time;
            $items[$column['day']]['dmt'] = $column->dmt;
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'items' => $items,
            ]);
        }
    }

    /**
     * Deletes an existing Timetable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Timetable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timetable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Timetable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            echo json_encode(ActiveForm::validate($model));
            \Yii::$app->end();
        }
    }
}

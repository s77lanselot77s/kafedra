<?php

use yii\helpers\Html;


?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Здравствуйте, <?= $user->shortFio ?>!<br>Кафедра вычислительной техники <a href="<?= Yii::$app->request->hostInfo ?>"><?= Yii::$app->request->hostInfo ?></a> уведомляет вас о новом личном сообщении от: <strong>"<?= $userTo->getProfile->shortFio ?>"</strong><br>
    Просмотреть сообщения в теме вы можете по <a href="<?= Yii::$app->request->hostInfo ?>/forum/topic/<?= $subscribe->topic->id ?>">ссылке</a>
</p>
<?php

use yii\helpers\Html;


?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Здравствуйте, <strong><?= $lab->student->shortFio ?></strong>!<br>Кафердра вычислительной техники уведомляет вас о том, что <?= $lab->labs->tsg->teacher->shortFio ?> принял вашу лабораторную работу "<?= $lab->labs->title ?>", оценив её в <strong><?= $lab->points ?></strong> баллов<br>
</p>
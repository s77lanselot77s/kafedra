<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$this->registerJsFile(Yii::getAlias('@web/js/jquery.session.js'),
    ['depends' => [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'],
        'position' => \yii\web\View::POS_HEAD,
    ]);
$this->registerJsFile('//cdn.jsdelivr.net/jquery.scrollto/2.1.2/jquery.scrollTo.min.js',
    ['depends' => [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'],
        'position' => \yii\web\View::POS_HEAD,
    ]);

?>
<?php
$this->registerJs("
    if($.session.get('left-menu') == undefined) {
        $('.l-menu').hide();
        console.log($('.block-content').hasClass('col-lg-9'))
        $('.block-content').removeClass('col-lg-9');
        $('.block-content').addClass('col-lg-12');
    } else {
        $('.block-content').removeClass('col-lg-12');
        $('.block-content').addClass('col-lg-9');
    }
    $('.left-menu').on('click', function(){

        if($('.l-menu').is(':visible')) {
            $.session.remove('left-menu');
            $('.block-content').removeClass('col-lg-9');
            $('.block-content').addClass('col-lg-12');
        }
        else {
            $.session.set('left-menu', 'true');
            $('.block-content').removeClass('col-lg-12');
            $('.block-content').addClass('col-lg-9');
        }
        $('.l-menu').slideToggle(300);

    });
", \yii\web\View::POS_READY);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Кафедра вычислительной техники',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Главная', 'url' => ['/site/index']],
                    ['label' => 'О кафедре', 'url' => ['/site/about']],
                    Yii::$app->user->isGuest ?
                        ['label' => 'Вход', 'url' => ['/user/security/login']] :
                        ['label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/user/security/logout'],
                            'linkOptions' => ['data-method' => 'post']],
/*                    ['label' => 'Регистрация', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest]*/


                ],
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?php if(!Yii::$app->user->isGuest): ?>
                <div class="row menu-abs">
                    <div class="col-lg-12">
                        <div class="left-menu glyphicon glyphicon-menu-hamburger"></div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(Yii::$app->user->isGuest): ?>
                <?= $content ?>
            <?php else: ?>
                <div class="row">
                    <div class="col-lg-3 l-menu">
                        <?php if(Yii::$app->user->can("student")): ?>
                            <?php
                            echo Nav::widget([
                                'items' => [
                                    [
                                        'label' => 'Главная',
                                        'url' => ['/site/index'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Профиль',
                                        'url' => ['/user/profile'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Лабораторные работы',
                                        'items' => [
                                            ['label' => 'Задания', 'url' => '/labs/index'],
                                            '<li class="divider"></li>',
                                            ['label' => 'Работы', 'url' => '/labs-students/index'],
                                        ],
                                    ],
                                    [
                                        'label' => 'Настройки',
                                        'url' => ['/user/settings'],
                                        'linkOptions' => [],
                                    ],
                                    /*[
                                        'label' => 'Учебный процесс',
                                        'items' => [
                                            ['label' => 'Курсовые', 'url' => '#'],
                                            '<li class="divider"></li>',
                                            '<li class="dropdown-header">заголовок в списке</li>',
                                            ['label' => 'Зачеты', 'url' => '#'],
                                            ['label' => 'Экзамены', 'url' => '#'],
                                        ],
                                    ],*/
                                ],
                                'options' => ['class' =>'nav-stacked'], // set this to nav-tab to get tab-styled navigation
                            ]);
                            ?>
                        <?php endif; ?>
                        <?php if(Yii::$app->user->can("teacher")): ?>
                            <?php
                            echo Nav::widget([
                                'items' => [
                                    [
                                        'label' => 'Главная',
                                        'url' => ['/site/index'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Настройки',
                                        'url' => ['/user/settings'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Профиль',
                                        'url' => ['/user/profile'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Студенты',
                                        'url' => ['/students/index'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Сообщения',
                                        'url' => ['/messages/index'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Журналы',
                                        'url' => ['/journal/index'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Лабораторные работы',
                                        'items' => [
                                            ['label' => 'Задания', 'url' => '/labs/index'],
                                            '<li class="divider"></li>',
                                            ['label' => 'Работы студентов', 'url' => '/labs-students/index'],
                                        ],
                                    ],
                                    [
                                        'label' => 'Управление новостями',
                                        'url' => ['/news/index'],
                                        'linkOptions' => [],
                                    ],
                                    [
                                        'label' => 'Шаблоны на КП',
                                        'url' => ['/cworks-default/index'],
                                        'linkOptions' => [],
                                    ],
                                ],
                                'options' => ['class' =>'nav-stacked'], // set this to nav-tab to get tab-styled navigation
                            ]);
                            ?>
                        <?php endif; ?>
                            <?php if(Yii::$app->user->can("admin")): ?>
                            <?php
                                echo Nav::widget([
                                    'items' => [
                                        [
                                            'label' => 'Главная',
                                            'url' => ['/site/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Профиль',
                                            'url' => ['/user/profile'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Настройки',
                                            'url' => ['/user/settings'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Расписание',
                                            'url' => ['/timetable/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Сообщения',
                                            'url' => ['/messages/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Добавление пользователя',
                                            'items' => [
                                                ['label' => 'Добавления преподавателя', 'url' => '/user/admin/create-teacher'],
                                                '<li class="divider"></li>',
                                                ['label' => 'Добавление студента', 'url' => '/user/admin/create-student'],
                                            ],
                                        ],
                                        [
                                            'label' => 'Лабораторные работы',
                                            'items' => [
                                                ['label' => 'Задания', 'url' => '/labs/index'],
                                                '<li class="divider"></li>',
                                                ['label' => 'Работы студентов', 'url' => '/labs-students/index'],
                                            ],
                                        ],
                                        [
                                            'label' => 'Дисциплины',
                                            'url' => ['/subject/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Учебное планирование',
                                            'url' => ['/subject-branches/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Курсовое проектирование',
                                            'items' => [
                                                ['label' => 'Бланки', 'url' => '/cworks/index'],
                                                '<li class="divider"></li>',
                                                ['label' => 'Шаблоны', 'url' => '/cworks-default/index'],
                                            ],
                                        ],
                                        [
                                            'label' => 'Распределение преподавателей',
                                            'url' => ['/tsg/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Специальности',
                                            'url' => ['/branches/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Журналы',
                                            'url' => ['/journal/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Группы',
                                            'url' => ['/groups/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Студенты',
                                            'url' => ['/students/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Преподаватели',
                                            'url' => ['/teachers/index'],
                                            'linkOptions' => [],
                                        ],
                                        [
                                            'label' => 'Управление новостями',
                                            'url' => ['/news/index'],
                                            'linkOptions' => [],
                                        ],
                                    ],
                                    'options' => ['class' =>'nav-stacked'], // set this to nav-tab to get tab-styled navigation
                                ]);
                            ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-9 block-content">
                        <?= $content ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Кафедра вычислительной техники | Created by: <?= Html::a('Alexandr Eremin', 'http://startdev.ru', ['target'=>'_blank']) ?> <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

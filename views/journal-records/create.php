<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JournalRecords */

$this->title = 'Create Journal Records';
$this->params['breadcrumbs'][] = ['label' => 'Journal Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journal-records-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

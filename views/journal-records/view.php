<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JournalRecords */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Journal Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journal-records-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'journal_id',
            'group_id',
            'subject_id',
            'student_id',
            'value',
            'date',
            'create_at',
            'update_at',
        ],
    ]) ?>

</div>

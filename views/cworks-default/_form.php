<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\CworksDefault */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cworks-default-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->user->can('teacher')): ?>
    <?= $form->field($model, 'tsg_id')->dropDownList(ArrayHelper::map(\app\models\TeacherSubjectGroups::find()->joinWith('subjectBranch')->where("teacher_id=".Yii::$app->user->id." AND (subject_branches.course_work=1 OR subject_branches.course_project=1) AND subject_branches.year=".date('Y'))->all(), 'id', 'planTeacher'), ['prompt'=>'Нет']); ?>
    <?php else: ?>
    <?= $form->field($model, 'tsg_id')->dropDownList(ArrayHelper::map(\app\models\TeacherSubjectGroups::find()->joinWith('subjectBranch')->where("`subject_branches`.`year`=".date('Y'))->all(), 'id', 'planTeacher'), ['prompt'=>'Нет']); ?>
    <?php endif; ?>
    <?= $form->field($model, 'ptitle')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'source_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'calculation_part')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'graphic_part')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'exper_part')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

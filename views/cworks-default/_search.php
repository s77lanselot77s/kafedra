<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CworksDefaultSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cworks-default-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tsg_id') ?>

    <?= $form->field($model, 'ptitle') ?>

    <?= $form->field($model, 'source_data') ?>

    <?= $form->field($model, 'calculation_part') ?>

    <?php // echo $form->field($model, 'graphic_part') ?>

    <?php // echo $form->field($model, 'exper_part') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

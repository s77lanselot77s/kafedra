<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CworksDefaultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Шаблоны на курсовое проектирование';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="cworks-default-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать новый шаблон КП', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'label'=>'__________Предмет/Преподаватель__________',
                'attribute'=> 'planTeacher',
                'value' => 'tsg.planTeacher',
                'format' => 'html',
                'filter' => Html::activeDropDownList($searchModel, 'tsg', ArrayHelper::map(\app\models\TeacherSubjectGroups::find()->all(), 'id', 'planTeacher'),['class'=>'form-control','prompt' => 'Не выбрано']),
            ],
            [
                'label'=>'__________Название работы__________',
                'attribute'=>'ptitle',
                'format'=>'text',
                'value'=>'ptitle',
            ],
            [
                'label'=>'__________Исходные данные__________',
                'attribute'=>'source_data',
                'value'=>'source_data',
            ],
            [
                'label'=>'__________Расчетная часть__________',
                'attribute'=>'calculation_part',
                'format'=>'text',
                'value'=>'calculation_part',
            ],
            [
                'label'=>'__________Графическая часть__________',
                'attribute'=>'calculation_part',
                'format'=>'text',
                'value'=>'graphic_part',
            ],
            [
                'label'=>'__________Экспериментальная часть__________',
                'attribute'=>'exper_part',
                'format'=>'text',
                'value'=>'exper_part',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CworksDefault */

$this->title = $model->ptitle;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны на курсовое проектирование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="cworks-default-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Выдействиетльно хотите удалить данный шаблон?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tsg.planTeacher',
            'ptitle:ntext',
            'source_data:ntext',
            'calculation_part:ntext',
            'graphic_part:ntext',
            'exper_part:ntext',
        ],
    ]) ?>

</div>

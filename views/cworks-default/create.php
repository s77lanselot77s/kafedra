<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CworksDefault */

$this->title = 'Добавление шаблона КП';
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны КП', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cworks-default-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'teacher' => $teacher,
    ]) ?>

</div>

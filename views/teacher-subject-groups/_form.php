<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\TeacherSubjectGroups */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$this->registerJs("
    $('#teachersubjectgroups-subject_branch_id').on('change', function(){
        if($(this).val() !== '') {
        var id = $(this).val();
            $.ajax({
                type: 'POST',
                data : {'id':id},
                url: '/tsg/update-hours',
                success: function(data){
                    var result = jQuery.parseJSON(data);
                    $('.hours-lec').show();
                    $('.hours-lab').show();
                    $('.hours-lec span').text(result.hours_lec);
                    $('.hours-lab span').text(result.hours_lab);
                    $('.form-group.field-teachersubjectgroups-hours_lec').show();
                    $('.form-group.field-teachersubjectgroups-hours_lab').show();
                }
            });
            return false;
        } else {
            $('.hours-lec').hide();
            $('.hours-lab').hide();
            $('.form-group.field-teachersubjectgroups-hours_lec').hide();
            $('.form-group.field-teachersubjectgroups-hours_lab').hide();
        }
    });

    $('#teachersubjectgroups-hours_lec').on('keyup', function(){
        var set_hours_lec = parseInt($(this).val());
        var free_hours_lec = parseInt($('.hours-lec > span').text());
        if(set_hours_lec > free_hours_lec) {
            alert('Указано большее количество часов, чем доступно');
            $(this).val(free_hours_lec);
            return false;
        }
    });

    $('#teachersubjectgroups-hours_lab').on('keyup', function(){
        var set_hours_lab = parseInt($(this).val());
        var free_hours_lab = parseInt($('.hours-lab > span').text());
        if(set_hours_lab > free_hours_lab) {
            alert('Указано большее количество часов, чем доступно');
            $(this).val(free_hours_lab);
            return false;
        }
    });
", \yii\web\View::POS_READY);
?>

<div class="teacher-subject-groups-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'subject_branch_id')->dropDownList(ArrayHelper::map(\app\models\SubjectBranches::find()->all(), 'id', 'plan'), ['prompt'=>'Нет']); ?>

    <?= $form->field($model, 'teacher_id')->dropDownList(ArrayHelper::map(\app\models\Teachers::find()->all(), 'user_id', 'fio'), ['prompt'=>'Нет']); ?>

    <?= $form->field($model, 'hours_lec')->textInput() ?>
    <p class="hours-lec">Осталось: <span class="badge"></span></br></br></p>

    <?= $form->field($model, 'hours_lab')->textInput() ?>
    <p class="hours-lab">Осталось: <span class="badge"></span></br></br></p>

    <?/*= $form->field($model, 'hours_cw')->textInput() */?>

    <?/*= $form->field($model, 'year')->textInput() */?><!--

    --><?/*= $form->field($model, 'sem')->textInput() */?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

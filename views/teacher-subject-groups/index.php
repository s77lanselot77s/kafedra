<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TeacherSubjectGroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Распределение часов между преподавателями';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-subject-groups-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить статью распределения', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label'=>'__________Статья учебного плана__________',
                'attribute'=> 'plan',
                'value' => 'subjectBranch.plan',
                'format' => 'html',
                'filter' => Html::activeDropDownList($searchModel, 'subjectBranch', ArrayHelper::map(\app\models\SubjectBranches::find()->all(), 'id', 'plan'),['class'=>'form-control','prompt' => 'Не выбрано']),
            ],
            [
                'label' => 'Преподаватель',
                'attribute' => 'fio',
                'value' => 'teacher.fio'
            ],
            'year',
            'sem',
            [
                'label'=>'Лек. (часов)',
                'attribute'=>'hours_lec',
            ],
            [
                'label'=>'Лаб. (часов)',
                'attribute'=>'hours_lab',
            ],
            // 'hours_cw',
            [
                'label'=>'Дата добавл.',
                'attribute'=>'create_at',
                'value'=> function ($data) {
                    return Yii::$app->formatter->asDate($data['create_at'], 'medium');
                },
            ],
            [
                'label'=>'Дата изм.',
                'attribute'=>'update_at',
                'value'=> function ($data) {
                    return Yii::$app->formatter->asDate($data['update_at'], 'medium');
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>

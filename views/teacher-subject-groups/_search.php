<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\TeacherSubjectGroupsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-subject-groups-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'teacher_id') ?>

    <?= $form->field($model, 'subject_branch_id') ?>

    <?= $form->field($model, 'hours_lec') ?>

    <?php  echo $form->field($model, 'hours_lab') ?>

    <?php // echo $form->field($model, 'hours_cw') ?>

    <?php  echo $form->field($model, 'year') ?>

    <?= $form->field($model, 'sem') ?>

    <?php  echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

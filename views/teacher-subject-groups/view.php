<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TeacherSubjectGroups */

$this->title = $model->teacher->shortFio;
$this->params['breadcrumbs'][] = ['label' => 'Распределение часов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="teacher-subject-groups-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label'=>'Преподаватель',
                'attribute'=>'teacher.shortFio',
            ],
            [
                'label'=>'Статья уч. плана',
                'attribute'=>'subjectBranch.plan',
            ],
            'year',
            'sem',
            'hours_lec',
            'hours_lab',
            'hours_cw',
            [
                'attribute'=>'create_at',
                'value'=>Yii::$app->formatter->asDate($model->create_at, 'medium'),
            ],
            [
                'attribute'=>'update_at',
                'value'=>Yii::$app->formatter->asDate($model->update_at, 'medium'),
            ],
        ],
    ]) ?>

</div>

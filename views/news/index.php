<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Дорбавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            [
                'label'=>'Автор',
                'attribute'=>'author_id',
                'value'=> function($data) {
                    return $data->getAuthorFio();
                }
            ],
            [
                'attribute'=>'create_at',
                'value'=> function ($data) {
                    return Yii::$app->formatter->asDate($data['create_at'], 'medium');
                },
            ],
            [
                'attribute'=>'update_at',
                'value'=> function ($data) {
                    return Yii::$app->formatter->asDate($data['update_at'], 'medium');
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

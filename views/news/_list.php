<?php
/**
 * Created by PhpStorm.
 * User: Alexandr
 * Date: 01.09.2015
 * Time: 20:53
 */
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $model->title; ?></h3>
    </div>
    <div class="panel-body">
        <?php echo $model->text; ?>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-lg-6">
                <p><?php echo '<b>Автор:</b> ' . $model->getAuthorFio(); ?></p>
            </div>
            <div class="col-lg-6">
                <p style="text-align: right;"><?php echo '<b>Дата публикации:</b> ' . Yii::$app->formatter->asDate($model->create_at, 'medium'); ?></p>
            </div>
        </div>
    </div>
</div>
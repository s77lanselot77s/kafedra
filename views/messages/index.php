<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="messages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Написать сообщение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


            <?=  Tabs::widget([
                'items' => [
                    [
                        'label' => 'Все сообщения',
                        'content' => GridView::widget([
                            'dataProvider' => $dataProviderAll,
                            'layout'=>"{items}\n{pager}",
                            'columns' => [
                                [
                                    'label' => 'тема',
                                    'value' => 'subject',
                                ],
                                [
                                    'label' => 'сообщение',
                                    'value' => function($model) {
                                    return \app\models\Messages::truncation($model->body, 100);
                                }
                                ],
                                [
                                    'label' => 'отправитель',
                                    'value' => function($model) {
                                        return ($model->userFrom->id == Yii::$app->user->id) ? 'Вы' : $model->userFrom->username;
                                    }
                                ],
                                [
                                    'label' => 'получатель',
                                    'value' => function($model) {
                                        return ($model->userTo->id == Yii::$app->user->id) ? 'Вы' : $model->userTo->username;
                                    }
                                ],
                                [
                                    'label' => 'дата',
                                    'value' => function ($model) { return Yii::$app->formatter->asDatetime($model->create_at, 'php:d-m-Y H:i:s', 'date'); }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {delete}',
                                ],
                            ],
                        ]),
                        'active' => true,
                        'headerOptions' => [],
                        'options' => ['id' => 'messageAll'],
                    ],
                    [
                        'label' => 'Входящие',
                        'content' => GridView::widget([
                            'dataProvider' => $dataProviderInbox,
                            'layout'=>"{items}\n{pager}",
                            'columns' => [
                                [
                                    'label' => 'тема',
                                    'value' => 'subject',
                                ],
                                [
                                    'label' => 'сообщение',
                                    'value' => 'body',
                                ],
                                [
                                    'label' => 'отправитель',
                                    'value' => function($model) {
                                        return ($model->userFrom->id == Yii::$app->user->id) ? 'Вы' : $model->userFrom->username;
                                    }
                                ],
                                [
                                    'label' => 'получатель',
                                    'value' => function($model) {
                                        return ($model->userTo->id == Yii::$app->user->id) ? 'Вы' : $model->userTo->username;
                                    }
                                ],
                                [
                                    'label' => 'дата',
                                    'value' => function ($model) { return Yii::$app->formatter->asDatetime($model->create_at, 'php:d-m-Y H:i:s', 'date'); }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {delete}',
                                ],
                            ],
                        ]),
                        'headerOptions' => [],
                        'options' => ['id' => 'messageInbox'],
                    ],
                    [
                        'label' => 'Исходящие',
                        'content' => GridView::widget([
                            'dataProvider' => $dataProviderOutbox,
                            'layout'=>"{items}\n{pager}",
                            'columns' => [
                                [
                                    'label' => 'тема',
                                    'value' => 'subject',
                                ],
                                [
                                    'label' => 'сообщение',
                                    'value' => 'body',
                                ],
                                [
                                    'label' => 'отправитель',
                                    'value' => function($model) {
                                        return ($model->userFrom->id == Yii::$app->user->id) ? 'Вы' : $model->userFrom->username;
                                    }
                                ],
                                [
                                    'label' => 'получатель',
                                    'value' => function($model) {
                                        return ($model->userTo->id == Yii::$app->user->id) ? 'Вы' : $model->userTo->username;
                                    }
                                ],
                                [
                                    'label' => 'дата',
                                    'value' => function ($model) { return Yii::$app->formatter->asDatetime($model->create_at, 'php:d-m-Y H:i:s', 'date'); }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {delete}',
                                ],
                            ],

                        ]),
                        'headerOptions' => [],
                        'options' => ['id' => 'messageOutbox'],
                    ],
                ],
            ]);
            ?>


</div>

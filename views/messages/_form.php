<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Students;
use app\models\Teachers;
use app\models\Groups;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="messages-form">

    <?php $form = ActiveForm::begin(
        [
            'options'=>['enctype'=>'multipart/form-data'],
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ]
    ); ?>

    <?= $form->field($model, 'teachersTo')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Teachers::find()->where('user_id != :user', [':user' => Yii::$app->user->id])->all(), 'user_id', 'shortFio'),
        'options' => ['placeholder' => 'Выберите получателя ...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>


    <?= $form->field($model, 'groupsTo')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Groups::find()->all(), 'id', 'title'),
        'options' => ['placeholder' => 'Выберите получателя ...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'studentsTo')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Students::find()->where('user_id != :user', [':user' => Yii::$app->user->id])->all(), 'user_id', 'shortFio'),
        'options' => ['placeholder' => 'Выберите получателя ...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'attachFile')->widget(FileInput::className(), [
        'options'=> [],
        'pluginOptions'=>[
            'allowedFileExtensions'=>['jpg', 'jpeg', 'png', 'doc','docx','xls', 'xlsx', 'ppt', 'pptx', 'rar', 'zip', 'txt', 'odt', 'pdf', 'djvu'],
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'showPreview' => true,
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

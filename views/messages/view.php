<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Сообщения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messages-view">
    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить данное сообщение?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'отправитель',
                'value' => ($model->userFrom->id == Yii::$app->user->id) ? 'Вы' : $model->userFrom->username,
            ],
            [
                'label' => 'получатель',
                'value' => ($model->userTo->id == Yii::$app->user->id) ? 'Вы' : $model->userTo->username,
            ],
            'subject',
            'body:ntext',
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => (isset($model->file) AND file_exists(Yii::getAlias('@webroot') . '/uploads/personal_message/' . $model->file)) ? Html::a('Cкачать' . ' ['. $model->file .']', [\yii\helpers\Url::to('messages/download'), 'filename' => $model->file]) : 'Нет',
                //'value' => Yii::getAlias('@webroot') . '/uploads/personal_message/' . $model->file,
            ],
            [
                'attribute' => 'create_at',
                'value' => Yii::$app->formatter->asDatetime($model->create_at, 'php:d-m-Y H:i:s', 'date')
            ],
            //'update_at',
        ],
    ]) ?>

</div>

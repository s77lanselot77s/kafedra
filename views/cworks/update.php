<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cworks */

$this->title = 'Редактирование бланка на курсовое проектирование: ' . ' ' . $model->ptitle;
$this->params['breadcrumbs'][] = ['label' => 'Курсовое проектирование', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ptitle, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование бланка на КП';
?>
<div class="cworks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

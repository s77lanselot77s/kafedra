<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CworksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cworks-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tsg_id') ?>

    <?= $form->field($model, 'student_id') ?>

    <?= $form->field($model, 'cworks_default_id') ?>

    <?= $form->field($model, 'status_id') ?>

    <?php // echo $form->field($model, 'ptitle') ?>

    <?php // echo $form->field($model, 'source_data') ?>

    <?php // echo $form->field($model, 'calculation_part') ?>

    <?php // echo $form->field($model, 'graphic_part') ?>

    <?php // echo $form->field($model, 'exper_part') ?>

    <?php // echo $form->field($model, 'start_at') ?>

    <?php // echo $form->field($model, 'finish_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

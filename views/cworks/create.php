<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cworks */

$this->title = 'Добавить бланк курсового проекта';
$this->params['breadcrumbs'][] = ['label' => 'Курсовое проектирование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cworks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'student' => $student,
    ]) ?>

</div>

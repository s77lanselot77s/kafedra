<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cworks */

$this->title = $model->ptitle;
$this->params['breadcrumbs'][] = ['label' => 'Курсовое проектирование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="cworks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот бланк?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cworksDefault.tsg.planTeacher',
            [
                'label' => 'Студент',
                'attribute' => 'student.shortFio',
            ],
            //'cworks_default_id',
            [
                'label'=>'Статус',
                'value'=>$model->getStatus()[$model->status_id],
            ],
            'ptitle:ntext',
            'source_data:ntext',
            'calculation_part:ntext',
            'graphic_part:ntext',
            'exper_part:ntext',
            'start_at',
            'finish_at',
            [
                'attribute'=>'create_at',
                'value'=>Yii::$app->formatter->asDate($model->create_at, 'medium'),
            ],
            [
                'attribute'=>'update_at',
                'value'=>Yii::$app->formatter->asDate($model->update_at, 'medium'),
            ],
        ],
    ]) ?>
    <?php if(Yii::$app->user->can('admin') and $model->status_id == $model::STATUS_CONFIRM_TEACHER): ?>
        <?= Html::a('Подтвердить', ['zav-kaf-confirm', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отклонить', ['reject', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>

    <?php if(Yii::$app->user->can('teacher') and $model->status_id == $model::STATUS_WAITING_FOR_CONFIRM): ?>
        <?= Html::a('Подтвердить', ['teacher-confirm', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отклонить', ['reject', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->user->can('student') and $model->status_id == $model::STATUS_ADDED): ?>
        <?= Html::a('Отправить на проверку', ['send-for-confirm', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>

    <?= (\app\models\Cworks::showView($model->status_id)) ? Html::a('Посмотреть/Сохранить в Word', ['/cworks/export-word', 'id'=>$model->id]) : ''; ?>
</div>

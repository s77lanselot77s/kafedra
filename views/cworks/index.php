<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CworksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курсовое проектирование';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="cworks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить бланк на курсовое проектирование', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'label'=>'Студент',
                'attribute'=>'shortFio',
                'format' => 'raw',
                'value'=>function ($data) {
                    return Html::a($data->student->shortFio, '/students/view/'.$data->student_id, ['target' => '_blank']);
                },
            ],
            [
                'label'=>'__________Предмет/Преподаватель__________',
                'attribute'=> 'planTeacher',
                'value' => function ($data) {
                        return Html::a($data->tsg->planTeacher, '/tsg/view?id='.$data->tsg_id, ['target' => '_blank']);
                    },
                'format' => 'raw',
                'filter' => Html::activeDropDownList($searchModel, 'tsg', ArrayHelper::map(\app\models\TeacherSubjectGroups::find()->all(), 'id', 'planTeacher'),['class'=>'form-control','prompt' => 'Не выбрано']),
            ],
            [
                'label'=>'__________Название работы__________',
                'attribute'=>'ptitle',
                'format'=>'text',
                'value'=>'ptitle',
            ],
            [
                'label'=>'Статус',
                'attribute'=>'status_id',
                'value'=> function($data) {
                    return $data->getStatus()[$data->status_id];
                },
                'format'=>'raw',
                'filter'=>Html::activeDropDownList($searchModel, 'status_id', $searchModel->getStatus(), ['class'=>'form-control','prompt' => 'Не выбрано']),
            ],
            [
                'label'=>'__________Исходные данные__________',
                'attribute'=>'source_data',
                'value'=>'source_data',
            ],
            [
                'label'=>'__________Расчетная часть__________',
                'attribute'=>'calculation_part',
                'format'=>'text',
                'value'=>'calculation_part',
            ],
            [
                'label'=>'__________Графическая часть__________',
                'attribute'=>'calculation_part',
                'format'=>'text',
                'value'=>'graphic_part',
            ],
            [
                'label'=>'__________Экспериментальная часть__________',
                'attribute'=>'exper_part',
                'format'=>'text',
                'value'=>'exper_part',
            ],
             'start_at',
             'finish_at',
            [
                'attribute'=>'create_at',
                'value'=> function ($data) {
                    return Yii::$app->formatter->asDate($data['create_at'], 'medium');
                },
            ],
            [
                'attribute'=>'update_at',
                'value'=> function ($data) {
                    return Yii::$app->formatter->asDate($data['update_at'], 'medium');
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Cworks */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$this->registerJs("
    if('".$this->context->action->id."' == 'update')
            $('.field-cworks-cworks_default_id').show();
    $('#cworks-tsg_id').on('change', function(){
        if($(this).val() !== '') {
        var id = $(this).val();
            $.ajax({
                type: 'POST',
                data : {'id':id},
                url: '/cworks/update-default-list',
                success: function(data){
                    var result = jQuery.parseJSON(data);
                    $('#cworks-cworks_default_id').html(result);
                    $('.field-cworks-cworks_default_id').show();
                }
            });
            return false;
        } else {
            $('.field-cworks-cworks_default_id').hide();
        }
    });
", \yii\web\View::POS_READY);
?>
<div class="cworks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!Yii::$app->user->can('student')): ?>
    <?= $form->field($model, 'tsg_id')->dropDownList(ArrayHelper::map(\app\models\TeacherSubjectGroups::find()->all(), 'id', 'planTeacher'), ['prompt'=>'Нет']); ?>
    <?php else: ?>
    <?= $form->field($model, 'tsg_id')->dropDownList(ArrayHelper::map(\app\models\TeacherSubjectGroups::find()->joinWith('subjectBranch')->where("`subject_branches`.`branch_id`=".Yii::$app->user->identity->profile->group->branch->id." AND (`subject_branches`.`course_work`=1 OR `subject_branches`.`course_project`=1) AND `subject_branches`.`year`=".date('Y'))->all(), 'id', 'planTeacher'), ['prompt'=>'Нет']); ?>
    <?php endif; ?>
    <?php if(!Yii::$app->user->can('student')): ?>
    <?= $form->field($model, 'student_id')->dropDownList(ArrayHelper::map(\app\models\Students::find()->all(), 'user_id', 'shortFio'), ['prompt'=>'Нет']); ?>
    <?php endif; ?>
    <?= $this->context->action->id == 'create' ?  $form->field($model, 'cworks_default_id')->dropDownList([items],[options], ['prompt'=>'Нет']) : $form->field($model, 'cworks_default_id')->dropDownList(ArrayHelper::map(\app\models\CworksDefault::findAll(['tsg_id'=>$model->tsg_id]), 'id', 'ptitle'), ['prompt'=>'Нет']); ?>

    <?= $form->field($model, 'ptitle')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'source_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'calculation_part')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'graphic_part')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'exper_part')->textarea(['rows' => 6]) ?>

    <?= $form->field($model,'start_at')->widget(DatePicker::className(),[
        'dateFormat' => 'yyyy-MM-dd',
        'clientOptions' => []
    ]) ?>
   <!-- --><?/*= $form->field($model,'finish_at')->widget(DatePicker::className(),[
        'dateFormat' => 'dd-MM-yyyy',
        'clientOptions' => []]) */?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

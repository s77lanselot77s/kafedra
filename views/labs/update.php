<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Labs */

$this->title = 'Редактирование лабораторной работы: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Labs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="labs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TeacherSubjectGroups;
use app\models\Groups;
use kartik\field\FieldRange;
use kartik\spinner\Spinner;
use kartik\file\FileInput;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model app\models\Labs */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("
    $('#labs-tsg_id').on('change', function(){
        if($(this).val() !== '') {
        var id = $(this).val();
            $.ajax({
                type: 'POST',
                data : {'id':id},
                url: '/labs/update-groups',
                success: function(data){
                console.log(data);
                    var result = data;
                    $('#labs-group_id').removeAttr('disabled');
                    $('#labs-group_id').empty();
                    $('#labs-group_id').append(result);
                }
            });
            return false;
        } else {
            $('#labs-group_id').empty();
            $('#labs-group_id').append('<option value=\"\">Выберите группу</option>');
            $('#labs-group_id').attr('disabled', 'disabled');
        }
    });
", \yii\web\View::POS_READY);

?>

<div class="labs-form">
    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'tsg_id')->dropDownList(ArrayHelper::map(TeacherSubjectGroups::find()->where('teacher_id = :teacher', [':teacher' => Yii::$app->user->id])->all(), 'id', 'planTeacherWithTime'), ['prompt'=>'Нет']); ?>

    <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(Groups::find()->all(), 'id', 'title'), [ 'prompt' => 'Выберите группу', 'disabled' => !isset($model->group_id) ? 'disabled' : false]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'attachFile')->widget(FileInput::className(), [
        'options'=> [],
        'pluginOptions'=>[
            'allowedFileExtensions'=>['jpg', 'jpeg', 'png', 'doc','docx','xls', 'xlsx', 'ppt', 'pptx', 'rar', 'zip', 'txt', 'odt', 'pdf', 'djvu'],
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'showPreview' => true,
        ],
    ]);
    ?>

    <?= FieldRange::widget([
        'form' => $form,
        'model' => $model,
        'useAddons' => false,
        'separator' => '←  →',
        'label' => 'минимум - максимум баллов за работу',
        'attribute1' => 'min_point',
        'attribute2' => 'max_point',
        'template' => '{label}{widget}{error}',
        //'errorContainer' => 'error',
        'type' => FieldRange::INPUT_SPIN,
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

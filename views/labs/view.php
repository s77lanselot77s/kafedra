<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Labs */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Лабораторные работы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="labs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить данную работу?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Специальность',
                'value' => $model->tsg->subjectBranch->branch->title,
            ],
            [
                'label' => 'Дисциплина',
                'value' => $model->tsg->subjectBranch->subject->title,
            ],
            [
                'label' => 'Группа',
                'value' => $model->group->title,
            ],
            'title',
            'description:ntext',
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => (isset($model->file) AND file_exists(Yii::getAlias('@webroot') . '/uploads/labs/' . $model->file)) ? Html::a('Cкачать' . ' ['. $model->file .']', [\yii\helpers\Url::to('labs/download'), 'filename' => $model->file]) : 'Нет',
                //'value' => Yii::getAlias('@webroot') . '/uploads/personal_message/' . $model->file,
            ],
            'min_point',
            'max_point',
            [
                'label' => 'Дата добавления',
                'attribute' => 'create_at',
                'value' => Yii::$app->formatter->asDatetime($model->create_at, 'php:d-m-Y H:i:s', 'date')
            ],
            [
                'label' => 'Дата редактирования',
                'attribute' => 'update_at',
                'value' => Yii::$app->formatter->asDatetime($model->update_at, 'php:d-m-Y H:i:s', 'date')
            ],
        ],
    ]) ?>

</div>

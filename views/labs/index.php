<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LabsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лабораторные работы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="labs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'label' => 'специальность',
                'attribute' => 'planBranch',
                'value' => 'tsg.subjectBranch.branch.title',
            ],
            [
                'label' => 'дисциплина',
                'attribute' => 'planSubject',
                'value' => 'tsg.subjectBranch.subject.title',
            ],
            [
                'label' => 'год',
                'attribute' => 'year',
                'value' => 'tsg.year',
            ],
            [
                'label' => 'семестр',
                'attribute' => 'sem',
                'value' => 'tsg.sem',
            ],
            [
                'label' => 'группа',
                'attribute' => 'group',
                'value' => 'group.title',
            ],
            'title',
            'description:ntext',
            // 'file',
            // 'min_point',
            // 'max_point',
            // 'create_at',
            // 'update_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>

</div>

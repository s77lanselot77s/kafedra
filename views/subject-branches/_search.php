<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\SubjectBranchesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subject-branches-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'subject_id') ?>

    <?= $form->field($model, 'branch_id') ?>

    <?= $form->field($model, 'course_work') ?>

    <?= $form->field($model, 'course_project') ?>

    <?= $form->field($model, 'exam') ?>

    <?= $form->field($model, 'zachet') ?>

    <?= $form->field($model, 'create_at') ?>


    <?php  echo $form->field($model, 'year') ?>

    <?php  echo $form->field($model, 'sem') ?>

    <?php  echo $form->field($model, 'hours_lec') ?>

    <?php  echo $form->field($model, 'hours_lab') ?>

    <?php /* echo $form->field($model, 'hours_cw') */?>

    <?php  echo $form->field($model, 'practice_week') ?>

    <?php  echo $form->field($model, 'aud_hours') ?>

    <?php  echo $form->field($model, 'sam_hours') ?>

    <?php  echo $form->field($model, 'summary') ?>

    <?php  echo $form->field($model, 'et') ?>

    <?php  echo $form->field($model, 'update_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

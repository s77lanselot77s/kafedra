<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SubjectBranchesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Учебное планирование';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-branches-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить план на дисциплину', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'label'=>'Специальность',
                'attribute' => 'branch',
                'value' => 'branch.title'
            ],
            [
                'label'=>'Дисциплина',
                'attribute' => 'subject',
                'value' => 'subject.title'
            ],
            [
                'attribute'=>'course_work',
                'format' => 'html',
                'value' => function ($data) {
                    return ($data['course_work']) ? "<span class=\"glyphicon glyphicon-ok\"></span>":"<span class=\"glyphicon glyphicon-remove\"></span>";
                },
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['style' => 'text-align:center; font-size:18px;'];
                },
                'filter'=>false,
            ],
            [
                'attribute'=>'course_project',
                'format' => 'html',
                'value' => function ($data) {
                    return ($data['course_project']) ? "<span class=\"glyphicon glyphicon-ok\"></span>":"<span class=\"glyphicon glyphicon-remove\"></span>";
                },
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['style' => 'text-align:center; font-size:18px;'];
                },
                'filter'=>false,
            ],
            [
                'attribute'=>'exam',
                'format' => 'html',
                'value' => function ($data) {
                    return ($data['exam']) ? "<span class=\"glyphicon glyphicon-ok\"></span>":"<span class=\"glyphicon glyphicon-remove\"></span>";
                },
                'contentOptions' =>function ($model, $key, $index, $column){
	                    return ['style' => 'text-align:center; font-size:18px;'];
	            },
                'filter'=>false,
            ],
            [
                'attribute'=>'zachet',
                'format' => 'html',
                'value' => function ($data) {
                    return ($data['zachet']) ? "<span class=\"glyphicon glyphicon-ok\"></span>":"<span class=\"glyphicon glyphicon-remove\"></span>";
                },
                'contentOptions' =>function ($model, $key, $index, $column){
                    return ['style' => 'text-align:center; font-size:18px;'];
                },
                'filter'=>false,
            ],
             'year',
             [
                 'label'=>'Сем.',
                 'attribute'=>'sem',
                 'value'=>'sem',
             ],
            [
                'label'=>'Лек. часов',
                'attribute'=>'hours_lec',
                'value'=>'hours_lec',
            ],
            [
                'label'=>'Лаб. часов',
                'attribute'=>'hours_lab',
                'value'=>'hours_lab',
            ],
             //'hours_cw',
            [
                'label'=>'Практика (нед.)',
                'attribute'=>'practice_week',
                'value'=>'practice_week',
            ],
             'aud_hours',
             'sam_hours',
             'summary',
             'et',
            [
                'attribute'=>'create_at',
                'value'=> function ($data) {
                    return Yii::$app->formatter->asDate($data['create_at'], 'medium');
                },
            ],
            [
                'attribute'=>'update_at',
                'value'=> function ($data) {
                    return Yii::$app->formatter->asDate($data['update_at'], 'medium');
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>

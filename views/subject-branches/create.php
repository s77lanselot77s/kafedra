<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubjectBranches */

$this->title = 'Добавление нового плана';
$this->params['breadcrumbs'][] = ['label' => 'Учебное планирование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-branches-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

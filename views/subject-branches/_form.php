<?php
// Учебное планирование
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\SubjectBranches */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subject-branches-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'branch_id')->dropDownList(ArrayHelper::map(\app\models\Branches::find()->all(), 'id', 'title'), ['prompt'=>'Не указана']); ?>

    <?= $form->field($model, 'subject_id')->dropDownList(ArrayHelper::map(\app\models\Subject::find()->all(), 'id', 'title'), ['prompt'=>'Не указан']); ?>

    <?= $form->field($model, 'year')->textInput() ?>

    <?= $form->field($model, 'sem')->textInput() ?>

    <?= $form->field($model, 'course_work', array('options' => ['class' => 'inline']))->checkbox(['value' => true], false) ?>

    <?= $form->field($model, 'course_project', array('options' => ['class' => 'inline']))->checkbox(['value' => true], false) ?>

    <?= $form->field($model, 'exam', array('options' => ['class' => 'inline']))->checkbox(['value' => true], false) ?>

    <?= $form->field($model, 'zachet', array('options' => ['class' => 'inline']))->checkbox(['value' => true], false) ?>

    <?= $form->field($model, 'hours_lec')->textInput() ?>

    <?= $form->field($model, 'hours_lab')->textInput() ?>

    <?/*= $form->field($model, 'hours_cw')->textInput() */?>

    <?= $form->field($model, 'practice_week')->textInput() ?>

    <?= $form->field($model, 'aud_hours')->textInput() ?>

    <?= $form->field($model, 'sam_hours')->textInput() ?>

    <?= $form->field($model, 'et')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

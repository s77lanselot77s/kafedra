<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SubjectBranches */

$this->title = $model->subject->title;
$this->params['breadcrumbs'][] = ['label' => 'Учебное планирование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="subject-branches-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот план?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'subject_id',
                'value'=> $model->subject->title,
            ],
            [
                'attribute'=>'branch_id',
                'value'=> $model->branch->title,
            ],
            //'course_work',
            'year',
            'sem',
            'hours_lec',
            'hours_lab',
            //'hours_cw',
            'practice_week',
            'aud_hours',
            'sam_hours',
            'summary',
            'et',
            [
                'attribute' => 'create_at',
                'value' => Yii::$app->formatter->asDate($model->create_at, 'medium')
            ],
            [
                'attribute' => 'update_at',
                'value' => Yii::$app->formatter->asDate($model->update_at, 'medium')
            ]
        ],
    ]) ?>

</div>

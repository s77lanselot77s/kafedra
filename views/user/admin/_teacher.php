<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\widgets\ActiveForm       $form
 * @var app\models\Teachers $teacher
 */

?>

<?= $form->field($teacher, 'lname') ?>
<?= $form->field($teacher, 'fname') ?>
<?= $form->field($teacher, 'pname') ?>
<?= $form->field($teacher, 'degree') ?>

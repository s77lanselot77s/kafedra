<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View              $this
 * @var app\models\user\User $user
 * @var app\models\Teachers $teacherProfile
 * @var app\models\Students $studentProfile
 */

$this->title = Yii::t('user', $this->context->action->id == 'create-teacher' ? 'Регистрация нового преподавателя' : ($this->context->action->id == 'create-student' ? 'Регистрация нового студента' : 'Регистрация нового пользователя'));
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', [
    'module' => Yii::$app->getModule('user'),
]) ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="panel-body">
        <div class="alert alert-info">
            <?= Yii::t('user', 'Credentials will be sent to user by email') ?>.
            <?= Yii::t('user', 'If you want to be generate password automatically leave password field empty') ?>.
        </div>
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation'   => true,
            'enableClientValidation' => false
        ]); ?>

        <?= $this->render('_user', ['form' => $form, 'user' => $user]) ?>
        <?php if($this->context->action->id == 'create-teacher'): ?>
        <?= $this->render('_teacher', ['form' => $form, 'teacher' => $teacherProfile]); ?>
        <?php elseif($this->context->action->id == 'create-student'): ?>
        <?= $this->render('_student', ['form' => $form, 'student' => $studentProfile]); ?>
        <?php endif; ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

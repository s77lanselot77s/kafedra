<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */
use yii\helpers\ArrayHelper;
/**
 * @var yii\widgets\ActiveForm       $form
 * @var app\models\Students $student
 */

?>

<?= $form->field($student, 'lname') ?>
<?= $form->field($student, 'fname') ?>
<?= $form->field($student, 'pname') ?>
<?= $form->field($student, 'group_id')->dropDownList(ArrayHelper::map(\app\models\Groups::find()->all(), 'id', 'title'), ['prompt'=>'Не указана']); ?>

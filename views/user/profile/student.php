<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = empty($profile->shortFio) ? Html::encode($profile->user->username) : Html::encode($profile->shortFio);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Профиль пользователя</div>
                    <div class="panel-body">
                        <h4><?= $this->title ?></h4>
                        <ul style="padding: 0; list-style: none outside none;">
                            <li><i class="glyphicon glyphicon-user text-muted"></i> <?php echo Html::encode($profile->lname) . ' '; echo Html::encode($profile->fname) . ' '; echo (!empty($profile->pname)) ? Html::encode($profile->pname) : '' ?></li>
                            <?php if (!empty($profile->group_id)): ?>
                                <li><i class="glyphicon glyphicon-education text-muted"></i> <?= Html::encode($profile->group->title) ?></li>
                            <?php endif; ?>
                            <?php if (!empty($profile->user->email)): ?>
                                <li><i class="glyphicon glyphicon-envelope text-muted"></i> <?= Html::encode($profile->user->email) ?></li>
                            <?php endif; ?>
                            <li><i class="glyphicon glyphicon-time text-muted"></i> <?= Yii::t('user', 'Joined on {0, date}', $profile->user->created_at) ?></li>
                        </ul>
                    </div>
                 </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Статусы бланков</div>
                    <div class="panel-body">
                        <?php
                            $CwStatusesProvider = new \yii\data\ActiveDataProvider(['query'=> \app\models\Cworks::find()->joinWith('tsg')->where(['student_id' => Yii::$app->user->id])]);
                            $blanks = $CwStatusesProvider->getModels();
                        ?>
                        <?php foreach($blanks as $blank): ?>
                            <div class="blank">
                                <p class="title"><b>Название работы: </b> <?= Html::a($blank->ptitle, '/cworks/view/'.$blank->id, ['target' => '_blank']); ?></p>
                                <p class="status"><b>Статус: </b> <?= $blank->getStatus()[$blank->status_id]; ?></p>
                                <hr>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Учебный план группы: <b><?= $profile->group->title ?></b></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <?= GridView::widget([
                                'dataProvider' => new ActiveDataProvider([
                                    'query' => \app\models\SubjectBranches::find()->joinWith('branch')->where("`branches`.`id`=".$profile->group->branch->id." AND `subject_branches`.`year`=".date('Y')),
                                    'pagination' => [
                                        'pageSize' => 20,
                                    ],
                                ]),
                                //'filterModel' => $searchModel,
                                'columns' => [
                                    [
                                        'label'=>'Дисциплина',
                                        'attribute' => 'subject',
                                        'value' => 'subject.title'
                                    ],
                                    [
                                        'attribute'=>'course_work',
                                        'format' => 'html',
                                        'value' => function ($data) {
                                            return ($data['course_work']) ? "<span class=\"glyphicon glyphicon-ok\"></span>":"<span class=\"glyphicon glyphicon-remove\"></span>";
                                        },
                                        'contentOptions' =>function ($model, $key, $index, $column){
                                            return ['style' => 'text-align:center; font-size:18px;'];
                                        },
                                        'filter'=>false,
                                    ],
                                    [
                                        'attribute'=>'course_project',
                                        'format' => 'html',
                                        'value' => function ($data) {
                                            return ($data['course_project']) ? "<span class=\"glyphicon glyphicon-ok\"></span>":"<span class=\"glyphicon glyphicon-remove\"></span>";
                                        },
                                        'contentOptions' =>function ($model, $key, $index, $column){
                                            return ['style' => 'text-align:center; font-size:18px;'];
                                        },
                                        'filter'=>false,
                                    ],
                                    [
                                        'attribute'=>'exam',
                                        'format' => 'html',
                                        'value' => function ($data) {
                                            return ($data['exam']) ? "<span class=\"glyphicon glyphicon-ok\"></span>":"<span class=\"glyphicon glyphicon-remove\"></span>";
                                        },
                                        'contentOptions' =>function ($model, $key, $index, $column){
                                            return ['style' => 'text-align:center; font-size:18px;'];
                                        },
                                        'filter'=>false,
                                    ],
                                    [
                                        'attribute'=>'zachet',
                                        'format' => 'html',
                                        'value' => function ($data) {
                                            return ($data['zachet']) ? "<span class=\"glyphicon glyphicon-ok\"></span>":"<span class=\"glyphicon glyphicon-remove\"></span>";
                                        },
                                        'contentOptions' =>function ($model, $key, $index, $column){
                                            return ['style' => 'text-align:center; font-size:18px;'];
                                        },
                                        'filter'=>false,
                                    ],
                                    'year',
                                    [
                                        'label'=>'Сем.',
                                        'attribute'=>'sem',
                                        'value'=>'sem',
                                    ],
                                    [
                                        'label'=>'Лек. часов',
                                        'attribute'=>'hours_lec',
                                        'value'=>'hours_lec',
                                    ],
                                    [
                                        'label'=>'Лаб. часов',
                                        'attribute'=>'hours_lab',
                                        'value'=>'hours_lab',
                                    ],
                                    //'hours_cw',
                                    [
                                        'label'=>'Практика (нед.)',
                                        'attribute'=>'practice_week',
                                        'value'=>'practice_week',
                                    ],
                                    'aud_hours',
                                    'sam_hours',
                                    'summary',
                                    'et',
                                    [
                                        'attribute'=>'create_at',
                                        'value'=> function ($data) {
                                            return Yii::$app->formatter->asDate($data['create_at'], 'medium');
                                        },
                                    ],
                                    [
                                        'attribute'=>'update_at',
                                        'value'=> function ($data) {
                                            return Yii::$app->formatter->asDate($data['update_at'], 'medium');
                                        },
                                    ],

                                    ['class' => 'yii\grid\ActionColumn'],
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

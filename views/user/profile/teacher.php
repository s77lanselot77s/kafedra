<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Профиль пользователя</div>
                    <div class="panel-body">
                        <h4><?= $this->title ?></h4>
                        <ul style="padding: 0; list-style: none outside none;">
                            <li><i class="glyphicon glyphicon-user text-muted"></i> <?php echo Html::encode($profile->lname) . ' '; echo Html::encode($profile->fname) . ' '; echo (!empty($profile->pname)) ? Html::encode($profile->pname) : '' ?></li>
                            <?php if (!empty($profile->degree)): ?>
                                <li><i class="glyphicon glyphicon-education text-muted"></i> <?= Html::encode($profile->degree) ?></li>
                            <?php endif; ?>
                            <?php if (!empty($profile->user->email)): ?>
                                <li><i class="glyphicon glyphicon-envelope text-muted"></i> <?= Html::encode($profile->user->email) ?></li>
                            <?php endif; ?>
                            <li><i class="glyphicon glyphicon-time text-muted"></i> <?= Yii::t('user', 'Joined on {0, date}', $profile->user->created_at) ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Бланки на КП, ожидающие подтверждения</div>
                    <div class="panel-body">
                        <?php
                        $CwStatusesProvider = new \yii\data\ActiveDataProvider(['query'=> \app\models\Cworks::find()->joinWith('tsg')->where("`teacher_subject_branch`.`teacher_id`=".Yii::$app->user->id." AND `status_id`=".\app\models\Cworks::STATUS_WAITING_FOR_CONFIRM)]);
                        $blanks = $CwStatusesProvider->getModels();
                        ?>
                        <?php foreach($blanks as $blank): ?>
                            <div class="blank">
                                <p class="title"><b>Название работы: </b> <?= Html::a($blank->ptitle, '/cworks/view/'.$blank->id, ['target' => '_blank']); ?></p>
                                <p class="student"><b>Группа: </b> <?= $blank->student->group->title; ?> (<?= $blank->student->shortFio ?>) </p>
                                <hr>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Бланки на КП, ожидающие подтверждения</div>
                    <div class="panel-body">
                        <?php
                        $CwStatusesProvider = new \yii\data\ActiveDataProvider(['query'=> \app\models\Cworks::find()->joinWith('tsg')->where("`status_id`=".\app\models\Cworks::STATUS_CONFIRM_TEACHER)]);
                        $blanks = $CwStatusesProvider->getModels();
                        ?>
                        <?php foreach($blanks as $blank): ?>
                            <div class="blank">
                                <p class="title"><b>Название работы: </b> <?= Html::a($blank->ptitle, '/cworks/view/'.$blank->id, ['target' => '_blank']); ?></p>
                                <p class="teacher"><b>Руководитель: </b> <?= $blank->tsg->teacher->shortFio; ?></p>
                                <p class="student"><b>Группа: </b> <?= $blank->student->group->title; ?> (<?= $blank->student->shortFio ?>) </p>
                                <hr>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\widgets\TimetableWidget;

/* @var $this yii\web\View */
/* @var $group app\models\Groups */

$this->title = $group->title;
$this->params['breadcrumbs'][] = ['label' => 'Расписание', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= TimetableWidget::widget(['group' => $group->id]) ?>
<div class="timetable-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['create', 'group' => $group->id], ['class' => 'btn btn-primary']) ?>
    </p>

</div>

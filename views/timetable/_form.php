<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\widgets\MultipleInput;
use kartik\time\TimePicker;
use kartik\tabs\TabsX;
use yii\helpers\ArrayHelper;
use app\models\Teachers;

/* @var $this yii\web\View */
/* @var $model app\models\Timetable */
/* @var $form yii\widgets\ActiveForm */


$this->registerJs("

var tabDisabled = function(id) {
    if($(id).prop('checked')) {
    console.log('dcdc');
        $(id).parentsUntil('.tab-content').find('input').attr('disabled', 'disabled');
        $(id).parentsUntil('.tab-content').find('select').attr('disabled', 'disabled');
        $(id).parentsUntil('.tab-content').find('.js-input-plus').attr('disabled', 'disabled');
        $(id).removeAttr('disabled');
    } else {
        $(id).parentsUntil('.tab-content').find('input').removeAttr('disabled');
        $(id).parentsUntil('.tab-content').find('select').removeAttr('disabled');
        $(id).parentsUntil('.tab-content').find('.js-input-plus').removeAttr('disabled');
    }
}

tabDisabled('#timetable-dmt0');
tabDisabled('#timetable-dmt1');
tabDisabled('#timetable-dmt2');
tabDisabled('#timetable-dmt3');
tabDisabled('#timetable-dmt4');
tabDisabled('#timetable-dmt5');


$('[id^=\"timetable-dmt\"]').change(function() {
var elemId = $(this).attr('id');
    tabDisabled('#' + elemId);
});

", \yii\web\View::POS_READY)

?>


<div class="timetable-form">

    <?php $form = ActiveForm::begin(
        [
            'enableAjaxValidation'      => true,
            'enableClientValidation'    => false,
            'validateOnChange'          => false,
            'validateOnSubmit'          => true,
            'validateOnBlur'            => false,
        ]
    ); ?>

    <?php

    $items = [
        [
            'label'=>'Понедельник',
            'content'=> $form->field($model, 'timetable0')->widget(MultipleInput::className(), [
                'limit' => 15,
                'min' => 0,
                'columns' => [
                    [
                        'name'  => 'time',
                        'type'  => TimePicker::className(),
                        'title' => 'Время',
                        'value' => function($data) {
                            return $data['time'];
                        },
                        'options' => [
                            'pluginOptions' => [
                                'showMeridian' => false,
                                'minuteStep' => 5,
                            ],
                        ],

                        'headerOptions' => [
                            'style' => 'width: 250px;',
                            'class' => 'day-css-class'
                        ]
                    ],
                    [
                         'name'  => 'title',
                         'enableError' => true,
                         'title' => 'Название предмета',
                         'options' => [
                             'class' => 'input-priority'
                         ]
                    ],
                    [
                        'name'  => 'type',
                        'enableError' => true,
                        'title' => 'Тип',
                        'defaultValue' => 0,
                        'type'  => 'dropDownList',
                        'items' => $model->types,
                    ],
                    [
                        'name'  => 'room',
                        'enableError' => true,
                        'title' => 'Кабинет',
                        'options' => [
                            'class' => 'input-priority'
                        ]
                    ],
                    [
                        'name'  => 'teacher_id',
                        'type'  => 'dropDownList',
                        'title' => 'Преподаватель',
                        'defaultValue' => 1,
                        'items' => ArrayHelper::map(Teachers::find()->all(), 'user_id', 'shortFio')
                    ],
                    [
                        'name'  => 'week',
                        'type'  => 'dropDownList',
                        'title' => 'Неделя',
                        'defaultValue' => 0,
                        'items' => $model->weeks,
                    ],
                    /*
                    [
                        'name'  => 'comment',
                        'type'  => 'static',
                        'value' => function($data) {
                            return Html::tag('span', 'static content', ['class' => 'label label-info']);
                        },
                        'headerOptions' => [
                            'style' => 'width: 70px;',
                        ]
                    ]
                    */
                ]
            ])->label(false) . $form->field($model, 'timetable0[0][dmt]')->checkbox(['id' => 'timetable-dmt0', 'label' => 'день военной подготовки']),
            'active'=>true
        ],
        [
            'label'=>'Вторник',
            'content'=> $form->field($model, 'timetable1')->widget(MultipleInput::className(), [
                    'limit' => 15,
                    'min' => 0,
                    'columns' => [
                        [
                            'name'  => 'time',
                            'type'  => TimePicker::className(),
                            'title' => 'Время',
                            'value' => function($data) {
                                return $data['time'];
                            },
                            'options' => [
                                'pluginOptions' => [
                                    'showMeridian' => false,
                                    'minuteStep' => 5,
                                ],
                            ],

                            'headerOptions' => [
                                'style' => 'width: 250px;',
                                'class' => 'day-css-class'
                            ]
                        ],
                        [
                            'name'  => 'title',
                            'enableError' => true,
                            'title' => 'Название предмета',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'type',
                            'enableError' => true,
                            'title' => 'Тип',
                            'defaultValue' => 0,
                            'type'  => 'dropDownList',
                            'items' => $model->types,
                        ],
                        [
                            'name'  => 'room',
                            'enableError' => true,
                            'title' => 'Кабинет',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'teacher_id',
                            'type'  => 'dropDownList',
                            'title' => 'Преподаватель',
                            'defaultValue' => 1,
                            'items' => ArrayHelper::map(Teachers::find()->all(), 'user_id', 'shortFio')
                        ],
                        [
                            'name'  => 'week',
                            'type'  => 'dropDownList',
                            'title' => 'Неделя',
                            'defaultValue' => 0,
                            'items' => $model->weeks,
                        ],
                    ]
                ])->label(false) . $form->field($model, 'timetable1[0][dmt]')->checkbox(['id' => 'timetable-dmt1', 'label' => 'день военной подготовки']),
            'active'=>false
        ],
        [
            'label'=>'Среда',
            'content'=> $form->field($model, 'timetable2')->widget(MultipleInput::className(), [
                    'limit' => 15,
                    'min' => 0,
                    'columns' => [
                        [
                            'name'  => 'time',
                            'type'  => TimePicker::className(),
                            'title' => 'Время',
                            'value' => function($data) {
                                return $data['time'];
                            },
                            'options' => [
                                'pluginOptions' => [
                                    'showMeridian' => false,
                                    'minuteStep' => 5,
                                ],
                            ],

                            'headerOptions' => [
                                'style' => 'width: 250px;',
                                'class' => 'day-css-class'
                            ]
                        ],
                        [
                            'name'  => 'title',
                            'enableError' => true,
                            'title' => 'Название предмета',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'type',
                            'enableError' => true,
                            'title' => 'Тип',
                            'defaultValue' => 0,
                            'type'  => 'dropDownList',
                            'items' => $model->types,
                        ],
                        [
                            'name'  => 'room',
                            'enableError' => true,
                            'title' => 'Кабинет',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'teacher_id',
                            'type'  => 'dropDownList',
                            'title' => 'Преподаватель',
                            'defaultValue' => 1,
                            'items' => ArrayHelper::map(Teachers::find()->all(), 'user_id', 'shortFio')
                        ],
                        [
                            'name'  => 'week',
                            'type'  => 'dropDownList',
                            'title' => 'Неделя',
                            'defaultValue' => 0,
                            'items' => $model->weeks,
                        ],
                    ]
                ])->label(false) . $form->field($model, 'timetable2[0][dmt]')->checkbox(['id' => 'timetable-dmt2', 'label' => 'день военной подготовки']),
            'active'=>false
        ],
        [
            'label'=>'Четверг',
            'content'=> $form->field($model, 'timetable3')->widget(MultipleInput::className(), [
                    'limit' => 15,
                    'min' => 0,
                    'columns' => [
                        [
                            'name'  => 'time',
                            'type'  => TimePicker::className(),
                            'title' => 'Время',
                            'value' => function($data) {
                                return $data['time'];
                            },
                            'options' => [
                                'pluginOptions' => [
                                    'showMeridian' => false,
                                    'minuteStep' => 5,
                                ],
                            ],

                            'headerOptions' => [
                                'style' => 'width: 250px;',
                                'class' => 'day-css-class'
                            ]
                        ],
                        [
                            'name'  => 'title',
                            'enableError' => true,
                            'title' => 'Название предмета',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'type',
                            'enableError' => true,
                            'title' => 'Тип',
                            'defaultValue' => 0,
                            'type'  => 'dropDownList',
                            'items' => $model->types,
                        ],
                        [
                            'name'  => 'room',
                            'enableError' => true,
                            'title' => 'Кабинет',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'teacher_id',
                            'type'  => 'dropDownList',
                            'title' => 'Преподаватель',
                            'defaultValue' => 1,
                            'items' => ArrayHelper::map(Teachers::find()->all(), 'user_id', 'shortFio')
                        ],
                        [
                            'name'  => 'week',
                            'type'  => 'dropDownList',
                            'title' => 'Неделя',
                            'defaultValue' => 0,
                            'items' => $model->weeks,
                        ],
                    ]
                ])->label(false) . $form->field($model, 'timetable3[0][dmt]')->checkbox(['id' => 'timetable-dmt3', 'label' => 'день военной подготовки']),
            'active'=>false
        ],
        [
            'label'=>'Пятница',
            'content'=> $form->field($model, 'timetable4')->widget(MultipleInput::className(), [
                    'limit' => 15,
                    'min' => 0,
                    'columns' => [
                        [
                            'name'  => 'time',
                            'type'  => TimePicker::className(),
                            'title' => 'Время',
                            'value' => function($data) {
                                return $data['time'];
                            },
                            'options' => [
                                'pluginOptions' => [
                                    'showMeridian' => false,
                                    'minuteStep' => 5,
                                ],
                            ],

                            'headerOptions' => [
                                'style' => 'width: 250px;',
                                'class' => 'day-css-class'
                            ]
                        ],
                        [
                            'name'  => 'title',
                            'enableError' => true,
                            'title' => 'Название предмета',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'type',
                            'enableError' => true,
                            'title' => 'Тип',
                            'defaultValue' => 0,
                            'type'  => 'dropDownList',
                            'items' => $model->types,
                        ],
                        [
                            'name'  => 'room',
                            'enableError' => true,
                            'title' => 'Кабинет',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'teacher_id',
                            'type'  => 'dropDownList',
                            'title' => 'Преподаватель',
                            'defaultValue' => 1,
                            'items' => ArrayHelper::map(Teachers::find()->all(), 'user_id', 'shortFio')
                        ],
                        [
                            'name'  => 'week',
                            'type'  => 'dropDownList',
                            'title' => 'Неделя',
                            'defaultValue' => 0,
                            'items' => $model->weeks,
                        ],
                    ]
                ])->label(false) . $form->field($model, 'timetable4[0][dmt]')->checkbox(['id' => 'timetable-dmt4', 'label' => 'день военной подготовки']),
            'active'=>false
        ],
        [
            'label'=>'Суббота',
            'content'=> $form->field($model, 'timetable5')->widget(MultipleInput::className(), [
                    'limit' => 15,
                    'min' => 0,
                    'columns' => [
                        [
                            'name'  => 'time',
                            'type'  => TimePicker::className(),
                            'title' => 'Время',
                            'value' => function($data) {
                                return $data['time'];
                            },
                            'options' => [
                                'pluginOptions' => [
                                    'showMeridian' => false,
                                    'minuteStep' => 5,
                                ],
                            ],

                            'headerOptions' => [
                                'style' => 'width: 250px;',
                                'class' => 'day-css-class'
                            ]
                        ],
                        [
                            'name'  => 'title',
                            'enableError' => true,
                            'title' => 'Название предмета',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'type',
                            'enableError' => true,
                            'title' => 'Тип',
                            'defaultValue' => 0,
                            'type'  => 'dropDownList',
                            'items' => $model->types,
                        ],
                        [
                            'name'  => 'room',
                            'enableError' => true,
                            'title' => 'Кабинет',
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'teacher_id',
                            'type'  => 'dropDownList',
                            'title' => 'Преподаватель',
                            'defaultValue' => 1,
                            'items' => ArrayHelper::map(Teachers::find()->all(), 'user_id', 'shortFio')
                        ],
                        [
                            'name'  => 'week',
                            'type'  => 'dropDownList',
                            'title' => 'Неделя',
                            'defaultValue' => 0,
                            'items' => $model->weeks,
                        ],
                    ]
                ])->label(false) . $form->field($model, 'timetable5[0][dmt]')->checkbox(['id' => 'timetable-dmt5', 'label' => 'день военной подготовки']),
            'active'=>false
        ],
    ];

    ?>

    <?/*= var_dump($model->timetable5);die; */?>

    <?/*= $form->field($model, 'teacher_id')->textInput() */?>

    <?/*= $form->field($model, 'week')->textInput() */?>

    <?/*= $form->field($model, 'day')->textInput() */?>

    <?/*= $form->field($model, 'title')->textInput(['maxlength' => true]) */?>

    <?/*= $form->field($model, 'time')->textInput() */?>

    <?/*= $form->field($model, 'dmt')->textInput() */?>

    <?/*= $form->field($model, 'create_at')->textInput() */?>

    <?/*= $form->field($model, 'update_at')->textInput() */?>

    <?=
         TabsX::widget([
        'items'=>$items,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TimetableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расписание занятий';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timetable-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //'id',
            [
                'label' => 'группа',
                'value' => 'title',
            ],
            //'groups_id',
            //'teacher_id',
            //'week',
            //'day',
            // 'title',
            // 'time',
            // 'dmt',
            // 'create_at',
            // 'update_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' =>'{view} {update}',
                'buttons'=>[
                    'view'=>function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [Url::to('view'), 'id' => $model->id], [
                            'data-pjax' => 0,
                            'data-method' => 'post',
                        ]);
                    },
                    'update'=>function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [Url::to('create'), 'group' => $model->id], [
                            'data-pjax' => 0,
                            'data-method' => 'post',
                        ]);
                    }
                ]
            ],

        ],
    ]); ?>

</div>

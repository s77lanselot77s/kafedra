<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Timetable */

$this->title = 'Расписание: ' . $group->title;
$this->params['breadcrumbs'][] = ['label' => 'Расписание', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
    echo '<div role="alert" class="alert alert-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="timetable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

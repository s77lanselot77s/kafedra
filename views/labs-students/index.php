<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LabsStudentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лабораторные работы студентов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="labs-students-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(Yii::$app->user->can('student')): ?>
    <p>
        <?= Html::a('Добавить лабораторную работу для проверки', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <div class="labs-students-table table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'label' => 'специальность',
                'attribute' => 'planBranch',
                'value' => 'labs.tsg.subjectBranch.branch.title',
            ],
            [
                'label' => 'дисциплина',
                'attribute' => 'planSubject',
                'value' => 'labs.tsg.subjectBranch.subject.title',
            ],
            [
                'label' => 'год',
                'attribute' => 'year',
                'value' => 'labs.tsg.year',
            ],
            [
                'label' => 'семестр',
                'attribute' => 'sem',
                'value' => 'labs.tsg.sem',
            ],
            [
                'label' => 'Лабораторная работа',
                'attribute' => 'labs',
                'value' => 'labs.title',
            ],
            'description:ntext',
            [
                'label' => 'группа',
                'attribute' => 'groupTitle',
                'value' => 'labs.group.title',
                'visible' => Yii::$app->user->can('teacher') ? true : false,
            ],
            [
                'label' => 'студент',
                'attribute' => 'student',
                'value' => 'student.shortFio',
                'visible' => Yii::$app->user->can('teacher') ? true : false,
            ],
            [
                'label' => 'преподаватель',
                'attribute' => 'planTeacher',
                'value' => 'labs.tsg.teacher.shortFio',
                'visible' => Yii::$app->user->can('student') ? true : false,
            ],
            [
                'label' => 'Статус',
                'format' => 'raw',
                'attribute' => 'points',
                'value' => function($model) {
                    return ($model->points) ? 'Принята<br>(' . $model->points . ' баллов)' : 'Не принята';
                },
                'visible' => Yii::$app->user->can('teacher') ? true : false,
            ],
            //'file',
            // 'points',
            // 'create_at',
            // 'update_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>

</div>

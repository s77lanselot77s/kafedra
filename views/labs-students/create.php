<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LabsStudents */

$this->title = 'Добавление лабораторной работы для проверки';
$this->params['breadcrumbs'][] = ['label' => 'Лабораторные работы студентов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="labs-students-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

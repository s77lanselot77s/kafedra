<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LabsStudents */

$this->title = $model->labs->tsg->planTeacherWithTime . ' | ' . $model->labs->title;
$this->params['breadcrumbs'][] = ['label' => 'Лабораторные работы студентов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="labs-students-view">

    <h1><?= $model->labs->tsg->planTeacherWithTime . '<br><hr>' . $model->labs->title; ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'labs.title',
            [
                'label' => 'Студент',
                'attribute' => 'student.shortFio',
                'visible' => Yii::$app->user->can('teacher')
            ],
            'description:ntext',
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => (isset($model->file) AND file_exists(Yii::getAlias('@webroot') . '/uploads/labs_students/' . $model->file)) ? Html::a('Cкачать' . ' ['. $model->file .']', [\yii\helpers\Url::to('labs-students/download'), 'filename' => $model->file]) : 'Нет',
            ],            [
                'label' => 'Статус работы',
                'attribute' => 'points',
                'value' => ($model->points) ? 'Принята (' . $model->points . ' баллов)' : 'Не принята'
            ],
            [
                'label' => 'Дата добавления',
                'attribute' => 'create_at',
                'value' => Yii::$app->formatter->asDatetime($model->create_at, 'php:d-m-Y H:i:s', 'date')
            ],
            [
                'label' => 'Дата редактирования',
                'attribute' => 'update_at',
                'value' => Yii::$app->formatter->asDatetime($model->update_at, 'php:d-m-Y H:i:s', 'date')
            ],
        ],
    ]) ?>
    <?php if(Yii::$app->user->can('teacher') AND !$model->points): ?>
    <?php $form = ActiveForm::begin(['action' => ['labs-students/update', 'id' => $model->id]]); ?>
        <?= $form->field($model, 'points')->textInput()->label('Баллы за работу') ?>
        <div class="form-group">
            <?= Html::submitButton('Принять работу', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    <?php endif; ?>
</div>

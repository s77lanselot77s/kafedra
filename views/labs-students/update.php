<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LabsStudents */

$this->title = 'Редактирование л/р: ' . ' ' . $this->title = $model->labs->tsg->planTeacherWithTime . ' | ' . $model->student->shortFio;
$this->params['breadcrumbs'][] = ['label' => 'Л/р студентов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title = $model->labs->tsg->planTeacherWithTime . ' | ' . $model->labs->title . ' | ' . $model->student->shortFio, 'url' => ['view', 'id' => $model->id]];
?>
<div class="labs-students-update">

    <h1><?= $this->title = $model->labs->tsg->planTeacherWithTime . '<br><hr>' . $model->labs->title . (Yii::$app->user->can('teacher') ? ' | ' . $model->student->shortFio : '') ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

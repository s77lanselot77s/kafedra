<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use app\models\Labs;
use app\models\LabsStudents;


/* @var $this yii\web\View */
/* @var $model app\models\LabsStudents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="labs-students-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?php if(Yii::$app->user->can('student')): ?>
    <?php if($model->isNewRecord): ?>
    <?= $form->field($model, 'labs_id')->dropDownList(LabsStudents::getLabsAvailableSender()) ?>
    <?php else: ?>
    <?= $form->field($model, 'labs_id')->dropDownList(ArrayHelper::map(Labs::find()->with('labsStudents')->where('group_id = :group', [':group' => Yii::$app->user->identity->profile->group->id])->all(), 'id', 'title')) ?>
    <?php endif; ?>
    <?php endif; ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('Описание (примечание)') ?>

    <?php if(Yii::$app->user->can('student')): ?>
    <?= $form->field($model, 'attachFile')->widget(FileInput::className(), [
        'options'=> [],
        'pluginOptions'=>[
            'allowedFileExtensions'=>['jpg', 'jpeg', 'png', 'doc','docx','xls', 'xlsx', 'ppt', 'pptx', 'rar', 'zip', 'txt', 'odt', 'pdf', 'djvu'],
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'showPreview' => true,
        ],
    ]);
    ?>
    <?php endif; ?>

    <?php if(Yii::$app->user->can('teacher') AND !$model->points): ?>
    <?= $form->field($model, 'points')->textInput()->label('баллы за работу') ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

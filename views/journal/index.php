<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\JournalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\Journal */

$this->title = 'Журналы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить журнал', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            /*'tsg.planTeacherWithTime',*/
            [
                'attribute' => 'planBranch',
                'value' => 'tsg.subjectBranch.branch.title',
            ],
            [
                'attribute' => 'planSubject',
                'value' => 'tsg.subjectBranch.subject.title',
            ],
            [
                'attribute' => 'year',
                'value' => 'tsg.year',
            ],
            [
                'attribute' => 'sem',
                'value' => 'tsg.sem',
            ],

            //'description:ntext',
            // 'create_at',
            // 'update_at',
            [
                'label' => 'Просмотр',
                'format' => 'raw',
                'value' => function($model) {
                    $result = '';
                    foreach ($model->tsg->subjectBranch->branch->groups as $group) {
                        $result .= Html::a($group->title, [Url::to('/journal/view'), 'id' => $model->id, 'group' => $group->id], ['target' => 'blank']) . '<br>';
                    }
                    return !empty($result) ? $result : '';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use app\widgets\JournalWidget;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Journal */
/* @var $group app\models\Groups */

$this->title = $model->tsg->planTeacherWithTime . ' Группа: ' . $group->title;
$this->params['breadcrumbs'][] = ['label' => 'Журналы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css');
$this->registerJsFile('//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js',
    ['depends' => [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'],
        'position' => \yii\web\View::POS_HEAD,
    ]);
$this->registerJsFile('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js',
    ['depends' => [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'],
        'position' => \yii\web\View::POS_HEAD,
    ]);
$this->registerJs("
//$.noConflict();
$('.journal-values tr td.value').editable({
    emptytext: '',
    type: 'text',
    pk: 1,
    params: function(params) {  //params already contain `name`, `value` and `pk`
        var data = {};
        data['record'] = $(this).attr('record');
        data['group'] = $(this).attr('group');
        data['year'] = $(this).attr('year');
        data['month'] = $(this).attr('month');
        data['day'] = $(this).attr('day');
        data['student'] = $(this).attr('student');
        data['journal'] = $(this).attr('journal');
        data['value'] = params['value'];
        return data;
    },
    url:   '/journal-records/update',
    title: 'Введите значение',

});

", \yii\web\View::POS_READY);
?>



<div class="journal-view table">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
    <div class="col-lg-3 col-md-3 col-xs-4 col-sm-12">
    <p><strong>Быстый переход</strong></p>
    <?=  DatePicker::widget([
        'name' => 'check_issue_date',
        'value' => date('d-M', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select issue date ...'],
        'pluginOptions' => [
            'format' => 'dd-M',
            'todayHighlight' => true,
            'changeYear' => false,
            //'viewMode' => "decades, years, months','days",
            //'minViewMode' => 'months',
            'language' => 'ru',
        ],
        'pluginEvents' => [
            /*"show" => "function(e) {  # `e` here contains the extra attributes }",
            "hide" => "function(e) {  # `e` here contains the extra attributes }",
            "clearDate" => "function(e) {  # `e` here contains the extra attributes }",*/
            "changeDate" => "function(e) {
             var month = e.date.getMonth() + 1;
             console.log(e.date);
             console.log(e.date.getDate());
             console.log(month);
             $('.table-responsive').scrollTo('td[month=\"' + month + '\"][day= \"' + e.date.getDate() + '\"]', 800);

             }",
            /*"changeYear" => "function(e) {  # `e` here contains the extra attributes }",
            "changeMonth" => "function(e) {  # `e` here contains the extra attributes }",*/
        ],
    ]); ?>
    </div>
    </div>
    <br>
    <?= JournalWidget::widget(['students' => $students, 'journal' => $model, 'journalRecords' => $records, 'group' => $group->id]) ?>
    <hr>
    <p>Примечание:</p>
    <?= Html::encode($model->description) ?>
</div>

<?php
use yii\bootstrap\Dropdown;
use yii\bootstrap\Nav;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
$this->title = 'Кафедра вычислительной техники';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">

        </div>
        <div class="row">
            <div class="col-lg-12">
                <?= ListView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => \app\models\News::find(),
                        'pagination' => [
                            'pageSize' => 20,
                        ],
                    ]),
                    'layout' => '{items}{pager}',
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('/news/_list', ['model' => $model]);
                    },
                ]) ?>
            </div>
        </div>

    </div>
</div>

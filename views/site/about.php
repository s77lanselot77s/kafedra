<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'О кафедре';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Страница с информацией о кафедре.
    </p>

    <code><?= __FILE__ ?></code>
</div>

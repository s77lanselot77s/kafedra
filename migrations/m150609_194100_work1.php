<?php

use yii\db\Schema;
use yii\db\Migration;

class m150609_194100_work1 extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand("
            CREATE TABLE IF NOT EXISTS `branches` (
              `id` INT NOT NULL,
              `title` VARCHAR(256) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC))
            ENGINE = InnoDB;
            -- -----------------------------------------------------
            -- Table `teachers`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `teachers` (
              `id` INT NOT NULL,
              `user_id` INT NOT NULL,
              `fname` VARCHAR(64) NOT NULL,
              `lname` VARCHAR(64) NOT NULL,
              `pname` VARCHAR(64) NULL,
              `degree` VARCHAR(64) NULL,
              `create_at` INT NOT NULL,
              `update_at` VARCHAR(45) NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
              INDEX `user_id_idx` (`user_id` ASC),
              CONSTRAINT `techers_user_id`
                FOREIGN KEY (`user_id`)
                REFERENCES `user` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
            -- -----------------------------------------------------
            -- Table `groups`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `groups` (
              `id` INT NOT NULL,
              `title` VARCHAR(45) NOT NULL,
              `branch_id` INT NOT NULL,
              `curator_id` INT NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
              INDEX `branch_id_idx` (`branch_id` ASC),
              INDEX `curator_id_idx` (`curator_id` ASC),
              CONSTRAINT `groups_branch_id`
                FOREIGN KEY (`branch_id`)
                REFERENCES `branches` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `groups_curator_id`
                FOREIGN KEY (`curator_id`)
                REFERENCES `teachers` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
            -- -----------------------------------------------------
            -- Table `students`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `students` (
              `id` INT NOT NULL,
              `user_id` INT NOT NULL,
              `fname` VARCHAR(64) NOT NULL,
              `lname` VARCHAR(64) NOT NULL,
              `pname` VARCHAR(64) NULL,
              `group_id` INT NULL,
              `create_at` INT NOT NULL,
              `update_at` INT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
              INDEX `user_id_idx` (`user_id` ASC),
              INDEX `group_id_idx` (`group_id` ASC),
              CONSTRAINT `students_user_id`
                FOREIGN KEY (`user_id`)
                REFERENCES `user` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `students_group_id`
                FOREIGN KEY (`group_id`)
                REFERENCES `groups` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
            -- -----------------------------------------------------
            -- Table `subject`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `subject` (
              `id` INT NOT NULL,
              `title` VARCHAR(256) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC))
            ENGINE = InnoDB;
            -- -----------------------------------------------------
            -- Table `subject_branches`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `subject_branches` (
              `id` INT NOT NULL,
              `subject_id` INT NOT NULL,
              `branch_id` INT NOT NULL,
              `course_work` INT NULL DEFAULT 0,
              `create_at` INT NOT NULL,
              `update_at` INT NULL,
              `year` INT(4) NOT NULL,
              `sem` INT NOT NULL,
              `hours_lec` INT NOT NULL,
              `hours_lab` INT NOT NULL DEFAULT 0,
              `hourse_cw` INT NULL DEFAULT 0,
              `practice_week` INT NULL,
              `aud_hours` INT NULL,
              `sam_hours` INT NULL,
              `summary` INT NULL,
              `et` FLOAT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
              INDEX `subject_id_idx` (`subject_id` ASC),
              INDEX `branch_id_idx` (`branch_id` ASC),
              CONSTRAINT `subbranch_subject_id`
                FOREIGN KEY (`subject_id`)
                REFERENCES `subject` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `subbranch_branch_id`
                FOREIGN KEY (`branch_id`)
                REFERENCES `branches` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
            -- -----------------------------------------------------
            -- Table `teacher_subject_branch`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `teacher_subject_branch` (
              `id` INT NOT NULL,
              `teacher_id` INT NOT NULL,
              `subject_branch_id` INT NOT NULL,
              `sem` INT NULL,
              `hours_lec` INT NULL,
              `hours_lab` INT NULL,
              `hours_cw` INT NULL,
              `year` INT(4) NULL,
              `create_at` INT NOT NULL,
              `update_at` INT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
              INDEX `teacher_id_idx` (`teacher_id` ASC),
              INDEX `subject_group_id_idx` (`subject_branch_id` ASC),
              CONSTRAINT `tsg_teacher_id`
                FOREIGN KEY (`teacher_id`)
                REFERENCES `teachers` (`id`)
                ON DELETE RESTRICT
                ON UPDATE CASCADE,
              CONSTRAINT `tsg_subject_group_id`
                FOREIGN KEY (`subject_branch_id`)
                REFERENCES `subject_branches` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE)
            ENGINE = InnoDB;
            -- -----------------------------------------------------
            -- Table `cworks_default`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `cworks_default` (
              `id` INT NOT NULL,
              `subject_branch_id` INT NOT NULL,
              `ptitle` TEXT NOT NULL,
              `source_data` TEXT NOT NULL,
              `calculation_part` TEXT NOT NULL,
              `graphic_part` TEXT NOT NULL,
              `exper_part` TEXT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
              INDEX `subject_branch_id_idx` (`subject_branch_id` ASC),
              CONSTRAINT `cworks_def_subject_branch_id`
                FOREIGN KEY (`subject_branch_id`)
                REFERENCES `subject_branches` (`id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE)
            ENGINE = InnoDB;
            -- -----------------------------------------------------
            -- Table `cworks`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `cworks` (
              `id` INT NOT NULL,
              `tsg_id` INT NOT NULL,
              `student_id` INT NOT NULL,
              `cworks_default_id` INT NOT NULL,
              `status_id` INT NULL DEFAULT 0,
              `ptitle` TEXT NOT NULL,
              `source_data` TEXT NOT NULL,
              `calculation_part` TEXT NOT NULL,
              `graphic_part` TEXT NOT NULL,
              `exper_part` TEXT NULL,
              `start_at` DATE NOT NULL,
              `finish_at` DATE NOT NULL,
              `create_at` INT NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE INDEX `id_UNIQUE` (`id` ASC),
              INDEX `tsg_id_idx` (`tsg_id` ASC),
              INDEX `cworks_default_id_idx` (`cworks_default_id` ASC),
              INDEX `student_id_idx` (`student_id` ASC),
              CONSTRAINT `cworks_tsg_id`
                FOREIGN KEY (`tsg_id`)
                REFERENCES `teacher_subject_branch` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `cworks_student_id`
                FOREIGN KEY (`student_id`)
                REFERENCES `students` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `cworks_cworks_default_id`
                FOREIGN KEY (`cworks_default_id`)
                REFERENCES `cworks_default` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ")->execute();

    }

    public function down()
    {
        echo "Revert.\n";
    }
}

<?php

namespace app\behaviors;

use Imagine\Image\Box;
use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\base\Exception;
use yii\imagine\Image;

class FileUploadBehavior extends Behavior
{
    public $image = 'image';
    public $image_attribute = 'cover_image';
    public $path = '/web/uploads/';
    public $deleteFile = true;
    public $resize = [];
    public $fileName = 'rand';
    /*
     * 'height'=>null, 'width'=>null
     * */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'afterValidate',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterValidate($event)
    {
        $pathdir =  Yii::getAlias('@app') . $this->path;
        $img = UploadedFile::getInstance($this->owner, $this->image);
        if($img)
        {
            if(!empty($this->owner{$this->image_attribute}))
                try {
                    unlink($pathdir . $this->owner{$this->image_attribute});
                 } catch(\Exception $e) {}
            //$ext = array_pop(explode(".", $img->name));
            $ext = array_pop(explode(".", $img->name));
            if($this->fileName == 'rand')
                $this->owner{$this->image_attribute} = Yii::$app->security->generateRandomString().".{$ext}";
            else
                $this->owner{$this->image_attribute} = $this->owner{$this->fileName}.".{$ext}";
            FileHelper::createDirectory($pathdir);
            $path = $pathdir . $this->owner{$this->image_attribute};
            $img->saveAs($path);
        }
    }

    public function afterDelete($event)
    {
        if($this->deleteFile)
        {
            $pathdir =  Yii::getAlias('@app') . $this->path;
            if(!empty($this->owner{$this->image_attribute}))
                try {
                    unlink($pathdir . $this->owner{$this->image_attribute});
                } catch(\Exception $e) {}
        }
    }
}
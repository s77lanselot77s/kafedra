<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\JournalRecords;

/**
 * JournalRecordsSearch represents the model behind the search form about `app\models\JournalRecords`.
 */
class JournalRecordsSearch extends JournalRecords
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'journal_id', 'group_id', 'subject_id', 'student_id', 'create_at', 'update_at'], 'integer'],
            [['value', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JournalRecords::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'journal_id' => $this->journal_id,
            'group_id' => $this->group_id,
            'subject_id' => $this->subject_id,
            'student_id' => $this->student_id,
            'date' => $this->date,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}

<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cworks;

/**
 * CworksSearch represents the model behind the search form about `app\models\Cworks`.
 */
class CworksSearch extends Cworks
{
    public $tsg;
    public $planTeacher;

    public $student;
    public $shortFio;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tsg_id', 'student_id', 'cworks_default_id', 'status_id', 'create_at'], 'integer'],
            [['ptitle', 'source_data', 'calculation_part', 'graphic_part', 'exper_part', 'start_at', 'finish_at', 'tsg', 'planTeacher', 'student', 'shortFio'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cworks::find();
        $query->joinWith(['tsg', 'student']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['tsg'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['planTeacher'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC, 'teacher_subject_branch.subject_branch_id' => SORT_ASC, 'teacher_subject_branch.teacher_id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC, 'teacher_subject_branch.subject_branch_id' => SORT_DESC, 'teacher_subject_branch.teacher_id' => SORT_DESC],
            'label' => 'Статья плана распределения',
            'default' => SORT_ASC
        ];

        $dataProvider->sort->attributes['student'] = [
            'asc' => ['students.user_id' => SORT_ASC],
            'desc' => ['students.user_id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['shortFio'] = [
            'asc' => ['students.lname' => SORT_ASC, 'students.fname' => SORT_ASC, 'students.pname' => SORT_ASC],
            'desc' => ['students.lname' => SORT_DESC, 'students.fname' => SORT_DESC, 'students.pname' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            self::tableName() . '.id' => $this->id,
            'tsg_id' => $this->tsg_id,
            'student_id' => $this->student_id,
            'cworks_default_id' => $this->cworks_default_id,
            'status_id' => $this->status_id,
            'start_at' => $this->start_at,
            'finish_at' => $this->finish_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'ptitle', $this->ptitle])
            ->andFilterWhere(['like', 'source_data', $this->source_data])
            ->andFilterWhere(['like', 'calculation_part', $this->calculation_part])
            ->andFilterWhere(['like', 'graphic_part', $this->graphic_part])
            ->andFilterWhere(['like', 'exper_part', $this->exper_part])
            ->andFilterWhere(['like', 'teacher_subject_branch.id', $this->tsg])
            ->andFilterWhere(['like', 'students.lname', $this->shortFio]);

        return $dataProvider;
    }
}

<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LabsStudents;

/**
 * LabsStudentsSearch represents the model behind the search form about `app\models\LabsStudents`.
 */
class LabsStudentsSearch extends LabsStudents
{

    /**
     * @inheritdoc
     */
    public $labs;

    /**
     * @inheritdoc
     */
    public $tsg;

    /**
     * @inheritdoc
     */
    public $planTeacher;

    /**
     * @inheritdoc
     */
    public $planSubject;

    /**
     * @inheritdoc
     */
    public $planBranch;

    /**
     * @inheritdoc
     */
    public $student;

    /**
     * @inheritdoc
     */
    public $groupTitle;

    /**
     * @inheritdoc
     */
    public $year;

    /**
     * @inheritdoc
     */
    public $sem;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'labs_id', 'student_id' , 'year', 'sem'], 'integer'],
            [['description', 'file', 'labs', 'tsg', 'points', 'planTeacher', 'groupTitle', 'create_at', 'update_at', 'planSubject', 'planBranch', 'student'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LabsStudents::find();

        $query->joinWith(['labs', 'labs.tsg', 'labs.tsg.teacher', 'labs.tsg.subjectBranch', 'labs.tsg.subjectBranch.subject', 'student']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['labs'] = [
            'asc' => ['labs.id' => SORT_ASC],
            'desc' => ['labs.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['labs.tsg'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['labs.tsg.teacher'] = [
            'asc' => ['teachers.title' => SORT_ASC],
            'desc' => ['teachers.title' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['labs.tsg.subjectBranch'] = [
            'asc' => ['subject_branches.id' => SORT_ASC],
            'desc' => ['subject_branches.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['labs.tsg.subjectBranch.subject'] = [
            'asc' => ['subject.title' => SORT_ASC],
            'desc' => ['subject.title' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['labs.tsg.subjectBranch.branch'] = [
            'asc' => ['branches.id' => SORT_ASC],
            'desc' => ['branches.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['year'] = [
            'asc' => ['teacher_subject_branch.year' => SORT_ASC],
            'desc' => ['teacher_subject_branch.year' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['sem'] = [
            'asc' => ['teacher_subject_branch.sem' => SORT_ASC],
            'desc' => ['teacher_subject_branch.sem' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['student'] = [
            'asc' => ['student.title' => SORT_ASC],
            'desc' => ['student.title' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['year'] = [
            'asc' => ['teacher_subject_branch.year' => SORT_ASC],
            'desc' => ['teacher_subject_branch.year' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['sem'] = [
            'asc' => ['teacher_subject_branch.sem' => SORT_ASC],
            'desc' => ['teacher_subject_branch.sem' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['planTeacher'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC, 'teacher_subject_branch.subject_branch_id' => SORT_ASC, 'teacher_subject_branch.teacher_id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC, 'teacher_subject_branch.subject_branch_id' => SORT_DESC, 'teacher_subject_branch.teacher_id' => SORT_DESC],
            'label' => 'Статья плана распределения',
            'default' => SORT_ASC
        ];

        $dataProvider->sort->attributes['planSubject'] = [
            'asc' => ['subject.title' => SORT_ASC],
            'desc' => ['subject.title' => SORT_DESC],
            'label' => 'Предмет',
            'default' => SORT_ASC
        ];


        $dataProvider->sort->attributes['planBranch'] = [
            'asc' => ['branches.title' => SORT_ASC],
            'desc' => ['branches.title' => SORT_DESC],
            'label' => 'Специальность',
            'default' => SORT_ASC
        ];

        $dataProvider->sort->attributes['planTeacherWithTime'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC, 'teacher_subject_branch.subject_branch_id' => SORT_ASC, 'teacher_subject_branch.teacher_id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC, 'teacher_subject_branch.subject_branch_id' => SORT_DESC, 'teacher_subject_branch.teacher_id' => SORT_DESC],
            'label' => 'Статья плана распределения',
            'default' => SORT_ASC
        ];

        $dataProvider->sort->attributes['groupTitle'] = [
            'asc' => ['groups.title' => SORT_ASC],
            'desc' => ['groups.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'labs_id' => $this->labs_id,
            'student_id' => $this->student_id,
            'points' => $this->points,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
        ->andFilterWhere(['like', 'file', $this->file]);
        $query->andFilterWhere(['like', 'subject.title', $this->planSubject]);
        $query->andFilterWhere(['like', 'teachers.title', $this->planTeacher]);
        $query->andFilterWhere(['like', 'teacher_subject_branch.id', $this->tsg]);
        $query->andFilterWhere(['like', 'teacher_subject_branch.year', $this->year]);
        $query->andFilterWhere(['like', 'teacher_subject_branch.sem', $this->sem]);
        $query->andFilterWhere(['like', 'branches.title', $this->planBranch]);
        $query->andFilterWhere(['like', 'groups.title', $this->groupTitle]);
        $query->andFilterWhere(['like', 'students.shortFio', $this->student]);

        return $dataProvider;
    }
}

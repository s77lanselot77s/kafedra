<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubjectBranches;

/**
 * SubjectBranchesSearch represents the model behind the search form about `app\models\SubjectBranches`.
 */
class SubjectBranchesSearch extends SubjectBranches
{
    public $subject;
    public $branch;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'subject_id', 'branch_id', 'course_work', 'course_project', 'exam', 'zachet', 'create_at', 'update_at', 'year', 'sem', 'hours_lec', 'hours_lab', 'hours_cw', 'practice_week', 'aud_hours', 'sam_hours', 'summary'], 'integer'],
            [['et'], 'number'],
            [['subject', 'branch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubjectBranches::find();

        $query->joinWith(['subject', 'branch']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['subject'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['subject.title' => SORT_ASC],
            'desc' => ['subject.title' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['branch'] = [
            'asc' => ['branches.title' => SORT_ASC],
            'desc' => ['branches.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'subject_branches.id' => $this->id,
            'subject_id' => $this->subject_id,
            'branch_id' => $this->branch_id,
            'course_work' => $this->course_work,
            'cource_project' => $this->course_work,
            'zachet' => $this->course_work,
            'exam' => $this->course_work,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'year' => $this->year,
            'sem' => $this->sem,
            'hours_lec' => $this->hours_lec,
            'hours_lab' => $this->hours_lab,
            'hours_cw' => $this->hours_cw,
            'practice_week' => $this->practice_week,
            'aud_hours' => $this->aud_hours,
            'sam_hours' => $this->sam_hours,
            'summary' => $this->summary,
            'et' => $this->et,
        ])->andFilterWhere(['like', 'subject.title', $this->subject])
            ->andFilterWhere(['like', 'branches.title', $this->branch]);

        return $dataProvider;
    }
}

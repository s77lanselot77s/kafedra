<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Groups;

/**
 * GroupsSearch represents the model behind the search form about `app\models\Groups`.
 */
class GroupsSearch extends Groups
{
    public $branch;
    public $curator;
    public $fio;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'branch_id', 'curator_id'], 'integer'],
            [['title', 'branch', 'curator', 'fio'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Groups::find();
        $query->joinWith(['branch', 'curator']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->sort->attributes['branch'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['branches.title' => SORT_ASC],
            'desc' => ['branches.title' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['curator'] = [
            'asc' => ['teacher.fio' => SORT_ASC],
            'desc' => ['teacher.fio' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['fio'] = [
            'asc' => ['teachers.lname' => SORT_ASC, 'teachers.fname' => SORT_ASC, 'teachers.pname' => SORT_ASC],
            'desc' => ['teachers.lname' => SORT_DESC, 'teachers.fname' => SORT_DESC, 'teachers.pname' => SORT_DESC],
            'label' => 'ФИО куратора',
            'default' => SORT_ASC
        ];

        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params)
         * statement below
         */

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            self::tableName() . '.id' => $this->id,
            'branch_id' => $this->branch_id,
            'curator_id' => $this->curator_id,
        ]);

        $query->andFilterWhere(['like', self::tableName() . '.title', $this->title]);

        $query->andFilterWhere(['like', 'branches.title', $this->branch]);
        $fioArr = explode(" ", $this->fio);
        $query->andFilterWhere(['or',
           ($fioArr[0]) ? ['like', 'teachers.lname', $fioArr[0]] : '',
           ($fioArr[1]) ? ['like', 'teachers.fname', $fioArr[1]] : ['like', 'teachers.fname', $fioArr[0]],
           ($fioArr[2]) ? ['like', 'teachers.pname', $fioArr[2]] : ['like', 'teachers.pname', $fioArr[1]], ['like', 'teachers.pname', $fioArr[0]],
        ]);

        return $dataProvider;
    }
}

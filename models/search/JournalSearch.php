<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Journal;

/**
 * JournalSearch represents the model behind the search form about `app\models\Journal`.
 */
class JournalSearch extends Journal
{
    /**
     * @inheritdoc
     */
    public $tsg;

    /**
     * @inheritdoc
     */
    public $year;

    /**
     * @inheritdoc
     */
    public $sem;

    /**
     * @inheritdoc
     */
    public $planTeacher;

    /**
     * @inheritdoc
     */
    public $planSubject;

    /**
     * @inheritdoc
     */
    public $planBranch;

    /**
     * @inheritdoc
     */
    public $planTeacherWithTime;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tsg_id', 'year', 'sem'], 'integer'],
            [['planSubject', 'planBranch'], 'string'],
            [['description', 'tsg', 'planTeacher', 'planTeacherWithTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Journal::find();
        $query->joinWith(['tsg', 'tsg.subjectBranch', 'tsg.subjectBranch.subject', 'tsg.subjectBranch.branch']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['tsg'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['tsg.subjectBranch'] = [
            'asc' => ['subject_branches.id' => SORT_ASC],
            'desc' => ['subject_branches.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['tsg.subjectBranch.subject'] = [
            'asc' => ['subject.id' => SORT_ASC],
            'desc' => ['subject.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['tsg.subjectBranch.branch'] = [
            'asc' => ['branches.id' => SORT_ASC],
            'desc' => ['branches.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['year'] = [
            'asc' => ['teacher_subject_branch.year' => SORT_ASC],
            'desc' => ['teacher_subject_branch.year' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['sem'] = [
            'asc' => ['teacher_subject_branch.sem' => SORT_ASC],
            'desc' => ['teacher_subject_branch.sem' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['planTeacher'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC, 'teacher_subject_branch.subject_branch_id' => SORT_ASC, 'teacher_subject_branch.teacher_id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC, 'teacher_subject_branch.subject_branch_id' => SORT_DESC, 'teacher_subject_branch.teacher_id' => SORT_DESC],
            'label' => 'Статья плана распределения',
            'default' => SORT_ASC
        ];

        $dataProvider->sort->attributes['planSubject'] = [
            'asc' => ['subject.title' => SORT_ASC],
            'desc' => ['subject.title' => SORT_DESC],
            'label' => 'Предмет',
            'default' => SORT_ASC
        ];


        $dataProvider->sort->attributes['planBranch'] = [
            'asc' => ['branches.title' => SORT_ASC],
            'desc' => ['branches.title' => SORT_DESC],
            'label' => 'Специальность',
            'default' => SORT_ASC
        ];

        $dataProvider->sort->attributes['planTeacherWithTime'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC, 'teacher_subject_branch.subject_branch_id' => SORT_ASC, 'teacher_subject_branch.teacher_id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC, 'teacher_subject_branch.subject_branch_id' => SORT_DESC, 'teacher_subject_branch.teacher_id' => SORT_DESC],
            'label' => 'Статья плана распределения',
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            $this->tableName() . '.id' => $this->id,
            'tsg_id' => $this->tsg_id,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);
        $query->andFilterWhere(['like', 'teacher_subject_branch.id', $this->tsg]);
        $query->andFilterWhere(['like', 'teacher_subject_branch.year', $this->year]);
        $query->andFilterWhere(['like', 'subject.title', $this->planSubject]);
        $query->andFilterWhere(['like', 'branches.title', $this->planBranch]);
        $query->andFilterWhere(['like', 'teacher_subject_branch.sem', $this->sem]);


        return $dataProvider;
    }
}

<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CworksDefault;

/**
 * CworksDefaultSearch represents the model behind the search form about `app\models\CworksDefault`.
 */
class CworksDefaultSearch extends CworksDefault
{
    public $tsg;
    public $planTeacher;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tsg_id'], 'integer'],
            [['ptitle', 'source_data', 'calculation_part', 'graphic_part', 'exper_part', 'tsg', 'planTeacher'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CworksDefault::find();
        $query->joinWith(['tsg']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['tsg'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['planTeacher'] = [
            'asc' => ['teacher_subject_branch.id' => SORT_ASC, 'teacher_subject_branch.subject_branch_id' => SORT_ASC, 'teacher_subject_branch.teacher_id' => SORT_ASC],
            'desc' => ['teacher_subject_branch.id' => SORT_DESC, 'teacher_subject_branch.subject_branch_id' => SORT_DESC, 'teacher_subject_branch.teacher_id' => SORT_DESC],
            'label' => 'Статья плана распределения',
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            self::tableName().'.id' => $this->id,
            'tsg_id' => $this->tsg_id,
        ]);

        $query->andFilterWhere(['like', 'ptitle', $this->ptitle])
            ->andFilterWhere(['like', 'source_data', $this->source_data])
            ->andFilterWhere(['like', 'calculation_part', $this->calculation_part])
            ->andFilterWhere(['like', 'graphic_part', $this->graphic_part])
            ->andFilterWhere(['like', 'exper_part', $this->exper_part])
            ->andFilterWhere(['like', 'teacher_subject_branch.id', $this->tsg]);

        return $dataProvider;
    }
}

<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeacherSubjectGroups;

/**
 * TeacherSubjectGroupsSearch represents the model behind the search form about `app\models\TeacherSubjectGroups`.
 */
class TeacherSubjectGroupsSearch extends TeacherSubjectGroups
{
    public $teacher;
    public $subjectBranch;
    public $plan;
    public $fio;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'teacher_id', 'subject_branch_id', 'sem', 'hours_lec', 'hours_lab', 'hours_cw', 'year', 'create_at', 'update_at'], 'integer'],
            [['teacher', 'subjectBranch', 'plan', 'fio'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeacherSubjectGroups::find();
        $query->joinWith(['teacher', 'subjectBranch']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['teacher'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['teacher.fio' => SORT_ASC],
            'desc' => ['teacher.fio' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['subjectBranch'] = [
            'asc' => ['subject_branches.id' => SORT_ASC],
            'desc' => ['subject_branches.id' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['plan'] = [
            'asc' => ['subject_branches.id' => SORT_ASC, 'subject_branches.subject_id' => SORT_ASC, 'subject_branches.branch_id' => SORT_ASC],
            'desc' => ['subject_branches.id' => SORT_DESC, 'subject_branches.subject_id' => SORT_DESC, 'subject_branches.branch_id' => SORT_DESC],
            'label' => 'Статья плана распределения',
            'default' => SORT_ASC
        ];
        $dataProvider->sort->attributes['fio'] = [
            'asc' => ['teachers.lname' => SORT_ASC, 'teachers.fname' => SORT_ASC, 'teachers.pname' => SORT_ASC],
            'desc' => ['teachers.lname' => SORT_DESC, 'teachers.fname' => SORT_DESC, 'teachers.pname' => SORT_DESC],
            'label' => 'ФИО куратора',
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            $this->tableName().'.id' => $this->id,
            'teacher_id' => $this->teacher_id,
            'subject_branch_id' => $this->subject_branch_id,
            'sem' => $this->sem,
            'hours_lec' => $this->hours_lec,
            'hours_lab' => $this->hours_lab,
            'hours_cw' => $this->hours_cw,
            'year' => $this->year,
            $this->tableName().'.create_at' => $this->create_at,
            $this->tableName().'.update_at' => $this->update_at,
        ])->andFilterWhere(['like', 'teacher.fio', $this->teacher])
            ->andFilterWhere(['like', 'subject_branches.id', $this->subjectBranch]
        );
        $fioArr = explode(" ", $this->fio);
        $query->andFilterWhere(['or',
            ($fioArr[0]) ? ['like', 'teachers.lname', $fioArr[0]] : '',
            ($fioArr[1]) ? ['like', 'teachers.fname', $fioArr[1]] : ['like', 'teachers.fname', $fioArr[0]],
            ($fioArr[2]) ? ['like', 'teachers.pname', $fioArr[2]] : ['like', 'teachers.pname', $fioArr[1]], ['like', 'teachers.pname', $fioArr[0]],
        ]);

        return $dataProvider;
    }
}

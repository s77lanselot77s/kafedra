<?php
// Учебное планирование
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "subject_branches".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property integer $branch_id
 * @property integer $course_work
 * @property integer $course_project
 * @property integer $exam
 * @property integer $zachet
 * @property integer $create_at
 * @property integer $update_at
 * @property integer $year
 * @property integer $sem
 * @property integer $hours_lec
 * @property integer $hours_lab
 * @property integer $hours_cw
 * @property integer $practice_week
 * @property integer $aud_hours
 * @property integer $sam_hours
 * @property integer $summary
 * @property double $et
 *
 * @property CworksDefault[] $cworksDefaults
 * @property Branches $branch
 * @property Subject $subject
 * @property TeacherSubjectGroups[] $teacherSubjectGroups
 */
class SubjectBranches extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject_branches';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->summary = $this->hours_lec + $this->hours_lab + $this->sam_hours;
            return true;
        } else {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'sem', 'branch_id', 'subject_id'], 'required'],
            [['course_work', 'course_project', 'exam', 'zachet', 'create_at', 'update_at', 'year', 'sem', 'hours_lec', 'hours_lab', 'hours_cw', 'practice_week', 'aud_hours', 'sam_hours', 'summary'], 'integer'],
            [['et'], 'number'],
            [['id'], 'unique'],
            [['plan'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject_id' => 'Дисциплина',
            'branch_id' => 'Специальность',
            'course_work' => 'К/р',
            'course_project' => 'К/п',
            'exam' => 'Экзамен',
            'zachet' => 'Зачет',
            'year' => 'Год',
            'sem' => 'Семестр',
            'hours_lec' => 'Лекционных часов',
            'hours_lab' => 'Лабораторных часов',
            'hours_cw' => 'Часы на курсовое проектирование',
            'practice_week' => 'Практика (недель)',
            'aud_hours' => 'Ауд. часы в нед.',
            'sam_hours' => 'Сам. ч. в сем.',
            'summary' => 'Всего часов',
            'et' => 'Всего 3ЕТ',
            'plan' => 'План',
            'create_at' => 'Дата создания',
            'update_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCworksDefaults()
    {
        return $this->hasMany(CworksDefault::className(), ['subject_branch_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branches::className(), ['id' => 'branch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherSubjectGroups()
    {
        return $this->hasMany(TeacherSubjectGroups::className(), ['subject_branch_id' => 'id']);
    }

    public function getPlan()
    {
        return '(' . $this->branch->number . ') ' . $this->branch->title . ' — ' . $this->subject->title . ' | Год: '.$this->year.' | Сем. № '.$this->sem;
    }
}

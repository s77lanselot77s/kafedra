<?php

namespace app\models;

use app\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "students".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $fname
 * @property string $lname
 * @property string $pname
 * @property integer $group_id
 * @property integer $create_at
 * @property integer $update_at
 * @property string $shortFio
 *
 * @property Cworks[] $cworks
 * @property Groups $group
 * @property User $user
 */
class Students extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'students';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    public function afterDelete()
    {
        $user = User::findOne(array('id'=>$this->user_id));
        if (isset($user))
            $user->delete();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fname', 'lname'], 'required'],
            [['group_id', 'create_at', 'update_at'], 'integer'],
            [['fname', 'lname', 'pname'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID',
            'fname' => 'Имя',
            'lname' => 'Фамилия',
            'pname' => 'Отчество',
            'group_id' => 'Группа',
            'create_at' => 'Дата регистрации',
            'update_at' => 'Дата редактирования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCworks()
    {
        return $this->hasMany(Cworks::className(), ['student_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getShortFio()
    {
        return $this->lname . ' ' . mb_substr($this->fname,0,1,'UTF-8') . '. ' . mb_substr($this->pname,0,1,'UTF-8').'.';
    }
}

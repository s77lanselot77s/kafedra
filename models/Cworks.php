<?php

namespace app\models;

use app\models\search\CworksDefaultSearch;
use app\models\search\CworksSearch;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * This is the model class for table "cworks".
 *
 * @property integer $id
 * @property integer $tsg_id
 * @property integer $student_id
 * @property integer $cworks_default_id
 * @property integer $status_id
 * @property string $ptitle
 * @property string $source_data
 * @property string $calculation_part
 * @property string $graphic_part
 * @property string $exper_part
 * @property string $start_at
 * @property string $finish_at
 * @property integer $create_at
 * @property integer $update_at
 *
 * @property integer STATUS_ADDED
 * @property integer STATUS_WAITING_FOR_CONFIRM
 * @property integer STATUS_CONFIRM_TEACHER
 * @property integer STATUS_CONFIRM_ZAVKAF
 * @property integer STATUS_REJECTED
 *
 * @property CworksDefault $cworksDefault
 * @property Students $student
 * @property TeacherSubjectGroups $tsg
 */
class Cworks extends \yii\db\ActiveRecord
{

    const STATUS_ADDED = 0;
    const STATUS_WAITING_FOR_CONFIRM = 1;
    const STATUS_CONFIRM_TEACHER = 2;
    const STATUS_CONFIRM_ZAVKAF = 3;
    const STATUS_REJECTED = 4;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cworks';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tsg_id', 'cworks_default_id', 'ptitle', 'source_data', 'calculation_part', 'graphic_part', 'start_at'], 'required'],
            [['id', 'tsg_id', 'student_id', 'cworks_default_id', 'status_id', 'create_at', 'update_at'], 'integer'],
            [['ptitle', 'source_data', 'calculation_part', 'graphic_part', 'exper_part'], 'string'],
            //[['finish_at'], 'validateToDates'],
            [['id'], 'unique'],
            [['create_at', 'update_at'], 'safe']
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tsg_id' => 'Преподаватель/Предмет',
            'student_id' => 'Студент',
            'cworks_default_id' => 'Шаблон',
            'status_id' => 'Статус',
            'ptitle' => 'Заголовок',
            'source_data' => 'Исходные данные',
            'calculation_part' => 'Расчетная часть',
            'graphic_part' => 'Графическая часть',
            'exper_part' => 'Экспериментальная часть',
            'start_at' => 'Дата получения задания',
            'finish_at' => 'Дата сдачи',
            'create_at' => 'Дата добавления',
            'update_at' => 'Дата изменения',
        ];
    }

    public function validateToDates($attribute, $params)
    {

        if($this->start_at  > $this->finish_at) {
            $this->addError($attribute, 'Дата получения позднее чем дата сдачи');
        }
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCworksDefault()
    {
        return $this->hasOne(CworksDefault::className(), ['id' => 'cworks_default_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Students::className(), ['user_id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTsg()
    {
        return $this->hasOne(TeacherSubjectGroups::className(), ['id' => 'tsg_id']);
    }

    public function getStatus()
    {
        return array(
            self::STATUS_ADDED => 'Добавлено',
            self::STATUS_WAITING_FOR_CONFIRM => 'Ожидает проверки',
            self::STATUS_CONFIRM_TEACHER => 'Проверено преподавателем',
            self::STATUS_CONFIRM_ZAVKAF => 'Проверено заведующим кафедры',
            self::STATUS_REJECTED => 'Отклонено',
        );
    }

    public static function showView($status)
    {
        if (//(Yii::$app->user->can('admin') AND ($status != Cworks::STATUS_CONFIRM_TEACHER)) OR
            (Yii::$app->user->can('teacher') AND ($status != Cworks::STATUS_WAITING_FOR_CONFIRM)) OR
                (Yii::$app->user->can('student') AND ($status != Cworks::STATUS_CONFIRM_ZAVKAF)))
            return false;
        else
            return true;

    }

    /*public function exportWord()
    {
        $searchModel = new CworksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        var_dump(Yii::$app->request->queryParams);die;
        if(empty($dataProvider))
            throw new HttpException(404, 'Бланка с таким номером не существует');

        // Initalize the TBS instance
        $OpenTBS = new \hscstudio\export\OpenTBS; // new instance of TBS
        // Change with Your template kaka
        $template = Yii::getAlias('@web/uploads').'/cworks_templates/cworks_template.docx';
        $OpenTBS->LoadTemplate($template); // Also merge some [onload] automatic fields (depends of the type of document).
        //$OpenTBS->VarRef['modelName']= "Mahasiswa";
        $data = [];
        $no=1;
        foreach($dataProvider->getModels() as $mahasiswa){
            $data[] = [
                'no'=>$no++,
                'nama'=>$mahasiswa->nama,
                'nim'=>$mahasiswa->nim,
            ];
        }
        $OpenTBS->MergeBlock('data', $data);
        // Output the result as a file on the server. You can change output file
        $OpenTBS->Show(OPENTBS_DOWNLOAD, 'export.docx'); // Also merges all [onshow] automatic fields.
        exit;
    }*/
}

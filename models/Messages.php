<?php

namespace app\models;

use app\behaviors\FileUploadBehavior;
use Yii;
use app\models\user\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $user_from
 * @property integer $user_to
 * @property string $subject
 * @property string $body
 * @property string $file
 * @property integer $create_at
 * @property integer $update_at
 *
 * @property User $userFrom
 * @property User $userTo
 */
class Messages extends \yii\db\ActiveRecord
{

    /** @inheritdoc */
    public $attachFile;

    /** @inheritdoc */
    public $fileName;

    /** @inheritdoc */
    public $teachersTo;

    /** @inheritdoc */
    public $studentsTo;

    /** @inheritdoc */
    public $groupsTo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
            'fileUpload' => [
                'class' => FileUploadBehavior::className(),
                'image' => 'attachFile',
                'image_attribute' => 'file',
                'fileName' => 'fileName',
                'deleteFile' => false,
                'path' => '/web/uploads/personal_message/',
                //'resize'=>['height'=>300],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_from', 'user_to', 'subject', 'body'], 'required'],
            [['user_from', 'user_to', 'user_id', 'create_at', 'update_at'], 'integer'],
            [['body'], 'string'],
            [['teachersTo', 'studentsTo', 'groupsTo'], 'safe'],
            [['attachFile'], 'file', 'extensions'=>'jpg, jpeg, png, doc, docx, xls, xlsx, ppt, pptx, rar, zip, txt, odt, pdf, djvu', 'skipOnEmpty'=>true],
            [['subject', 'file', 'fileName'], 'string', 'max' => 128],
            [['groupsTo', 'teachersTo', 'studentsTo'], 'required', 'when' => function($model) {
                return (empty($model->studentTo) && empty($model->teachersTo) && empty($model->groupsTo));
            }, 'whenClient' => "function (attribute, value) {

                    if($('.field-messages-studentsto ul li.select2-selection__choice').attr('title') === undefined && $('.field-messages-teachersto ul li.select2-selection__choice').attr('title') === undefined && $('.field-messages-groupsto ul li.select2-selection__choice').attr('title') === undefined){
                        $('.field-messages-studentsto, .field-messages-teachersto, .field-messages-groupsto').removeClass( 'has-success' ).addClass( 'has-error' ).find('.help-block').text( 'Необходимо выбрать хотя бы одного получателя из любого списка');
                        return true;
                    } else {
                        $('.field-messages-studentsto, .field-messages-teachersto, .field-messages-groupsto').removeClass( 'has-error' ).find('.help-block').text('');
                        return false;
                    }
                }", 'message' => 'Необходимо выбрать хотя бы одного получателя из любого списка', 'on' => 'create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'user_from' => 'Отправитель',
            'user_to' => 'Получатель',
            'subject' => 'Тема',
            'body' => 'Сообщение',
            'file' => 'Файл',
            'teachersTo' => 'Преподаватели',
            'groupsTo' => 'Группы студентов',
            'studentsTo' => 'Отдельные студенты',
            'attachFile' => 'Файл',
            'create_at' => 'Дата',
            'update_at' => 'Update At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'user_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'user_to']);
    }

    /** @inheritdoc */
    public function sendMessageGroups($groups)
    {
        $userFrom = User::findOne(['id' => Yii::$app->user->id]);
        foreach ($groups as $group) {
            $groupModel = Groups::find()->joinWith(['students', 'students.user'])->where('id = :group', [':group' => $group]);
            foreach ($groupModel->students as $student) {
                $this->sendPM($userFrom, $student);
            }
        }
    }

    /** @inheritdoc */
    public function sendMessageStudents($students)
    {
        $userFrom = User::findOne(['id' => Yii::$app->user->id]);
        foreach ($students as $student) {
            $this->sendPM($userFrom, $student);
        }
    }

    /** @inheritdoc */
    public function sendMessageTeachers($teachers)
    {
        $userFrom = User::findOne(['id' => Yii::$app->user->id]);
        foreach ($teachers as $teacher) {
            $this->sendPM($userFrom, $teacher);
        }
    }

    /** @inheritdoc */
    public function saveMessage($users, $fileName = false)
    {

        foreach($users as $user)
        {
            if($user == Yii::$app->user->id)
                continue;
            $messageFrom = new Messages();
            $messageTo = new Messages();

            $messageFrom->user_from = Yii::$app->user->id;
            if($fileName) {
                $messageFrom->fileName = $fileName;
                $messageTo->fileName = $fileName;
            }

            $messageFrom->user_to = $user;
            $messageFrom->subject = $this->subject;
            $messageFrom->body = $this->body;
            $messageFrom->user_id = $messageFrom->user_from;

            $messageTo->user_from = Yii::$app->user->id;
            $messageTo->user_to = $user;
            $messageTo->subject = $this->subject;
            $messageTo->body = $this->body;
            $messageTo->user_id = $messageTo->user_to;
            if(!$messageFrom->save())
            {
                throw new HttpException(500, 'При сохранении сообщения произошла ошибка');
            }
            if(!$messageTo->save())
            {
                throw new HttpException(500, 'При сохранении сообщения произошла ошибка');
            }
        }
    }

    private function sendPM($userFrom, $userTo)
    {
        Yii::$app->mailer->compose('messages/new-message', ['from' => $userFrom, 'user' => $userTo])
            ->setFrom('admin@kafedra.dev')
            ->setTo($userTo->user->email)
            ->setSubject('Новое сообщение на сайте кафедры Вычислительной техники')
            ->send();
    }


    public static function truncation($str, $length)
    {
        $str=substr($str, 0, $length-2);        //Обрезаем до заданной длины
        $words=explode(" ", $str);                //Разбиваем по словам
        array_splice($words,-1);                //Удаляем последнее слово

        $last=array_pop($words);                //Получаем последнее слово

        for ($i=1; $i<strlen($last); $i++) {
            //Ищем и удаляем в конце последнего слова все кроме букв и цифр
            if (preg_match('/\W$/',substr($last,-1,1))) $last=substr($last,0,strlen($last)-1);
            else break;
        }
        return implode(" ", $words).' '.$last.'...';
    }

}

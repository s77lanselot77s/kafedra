<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\models\user\User;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $author_id
 * @property integer $create_at
 * @property integer $update_at
 *
 * @property User $author
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->author_id = Yii::$app->user->id;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['text'], 'string'],
            [['author_id', 'create_at', 'update_at'], 'integer'],
            [['title'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок новости',
            'text' => 'Текст новости',
            'author_id' => 'Автор',
            'create_at' => 'Дата публикации',
            'update_at' => 'Дата редактирования',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getAuthorFio()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->author_id);
        if(array_key_exists('admin', $roles)) {
            return 'Администратор';
        }
        if(array_key_exists('teacher', $roles)) {
            $user = Teachers::findOne(['user_id' => $this->author_id]);
            return $user->shortFio;
        }
        if(array_key_exists('student', $roles)) {
            $user = Students::findOne(['user_id' => $this->author_id]);
            return $user->shortFio;
        }

    }
}

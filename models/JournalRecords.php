<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "journal_records".
 *
 * @property integer $id
 * @property integer $journal_id
 * @property integer $group_id
 * @property integer $subject_id
 * @property integer $student_id
 * @property string $value
 * @property string $date
 * @property integer $create_at
 * @property integer $update_at
 *
 * @property Groups $group
 * @property Journal $journal
 * @property Students $student
 */
class JournalRecords extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'journal_records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['journal_id', 'group_id', 'student_id', 'date'], 'required'],
            [['journal_id', 'group_id', 'student_id', 'create_at', 'update_at'], 'integer'],
            [['date'], 'safe'],
            [['value'], 'string', 'max' => 45]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'journal_id' => 'Журнал',
            'group_id' => 'Группа',
            'subject_id' => 'Subject ID',
            'student_id' => 'студент',
            'value' => 'Значение',
            'date' => 'Дата',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJournal()
    {
        return $this->hasOne(Journal::className(), ['id' => 'journal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Students::className(), ['user_id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }
}

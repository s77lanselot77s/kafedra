<?php

namespace app\models;

use dektrium\user\models\Token;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "teacher_subject_branch".
 *
 * @property integer $id
 * @property integer $teacher_id
 * @property integer $subject_branch_id
 * @property integer $sem
 * @property integer $hours_lec
 * @property integer $hours_lab
 * @property integer $hours_cw
 * @property integer $year
 * @property integer $create_at
 * @property integer $update_at
 * @property string $planTeacher
 * @property string $planTeacherWithTime
 *
 * @property Cworks[] $cworks
 * @property SubjectBranches $subjectBranch
 * @property Teachers $teacher
 */
class TeacherSubjectGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teacher_subject_branch';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $plan = SubjectBranches::findOne(['id'=>$this->subject_branch_id]);
            $this->year = $plan->year;
            $this->sem = $plan->sem;
            return true;
        } else {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'subject_branch_id'], 'required'],
            [['id', 'teacher_id', 'subject_branch_id', 'sem', 'hours_lec', 'hours_lab', 'hours_cw', 'year', 'create_at', 'update_at'], 'integer'],
            [['id'], 'unique'],
            [['hours_lec', 'hours_lab'], 'validateHours'],
            [['create_at', 'update_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Преподаватель',
            'subject_branch_id' => 'План',
            'sem' => 'Сем.',
            'hours_lec' => 'Лекционных часов',
            'hours_lab' => 'Лабораторных часов',
            'hours_cw' => 'Часов на курсовое проектирование',
            'year' => 'Год',
            'planTeacher' => 'Предмет/Преподаватель',
            'planTeacherWithTime' => 'Предмет/Преподаватель/год/семестр',
            'create_at' => 'Дата добавления',
            'update_at' => 'Дата редактирования',
        ];
    }

    public function validateHours($attribute, $params)
    {
        $plan = SubjectBranches::findOne(['id'=>$this->subject_branch_id]);
        $models = TeacherSubjectGroups::findAll(['subject_branch_id'=>$this->subject_branch_id]);
        $hours = 0;
        foreach ($models as $model) {
            $hours += $model->{$attribute};
        }
        $hours += $attribute;
        if($hours > $plan->{$attribute})
            $this->addError($attribute, 'Указанное количество часов превышает суммарное выделенное по плану на данный предмет');

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getFreeHours($subject_branch_id)
    {
        $hours = array();
        $plan = SubjectBranches::findOne(['id'=>$subject_branch_id]);
        $models = TeacherSubjectGroups::findAll(['subject_branch_id'=>$subject_branch_id]);
        $hours_lec = $hours_lab = 0;
        foreach ($models as $model) {
            $hours_lec += $model->hours_lec;
            $hours_lab += $model->hours_lab;
        }
        $hours['hours_lec'] = $plan->hours_lec - $hours_lec;
        $hours['hours_lab'] = $plan->hours_lab - $hours_lab;
        return $hours;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCworks()
    {
        return $this->hasMany(Cworks::className(), ['tsg_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectBranch()
    {
        return $this->hasOne(SubjectBranches::className(), ['id' => 'subject_branch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teachers::className(), ['user_id' => 'teacher_id']);
    }

    public function getPlanTeacher()
    {
        return $this->subjectBranch->subject->title . ' / '.$this->teacher->shortFio;
    }

    public function getPlanTeacherWithTime()
    {
        return $this->subjectBranch->branch->title . ' ('. $this->subjectBranch->branch->number .') ' .  ' / ' . $this->subjectBranch->subject->title . ' / '.$this->teacher->shortFio . ' / ' . $this->year . ' / '. $this->sem . ' сем.';
    }

    /*public function getPlan()
    {
        return '(' . $this->subjectBranch->branch->number . ') ' . $this->subjectBranch->branch->title . ' — ' . $this->subjectBranch->subject->title . ' | Год: '.$this->subjectBranch->year.' | Сем. № '.$this->subjectBranch->sem;
    }*/
}

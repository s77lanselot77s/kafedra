<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\behaviors\FileUploadBehavior;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "labs_students".
 *
 * @property integer $id
 * @property integer $labs_id
 * @property integer $student_id
 * @property string $description
 * @property string $file
 * @property integer $points
 * @property integer $create_at
 * @property integer $update_at
 *
 * @property Students $student
 * @property Labs $labs
 */
class LabsStudents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'labs_students';
    }

    /**
     * @inheritdoc
     */
    public $attachFile;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
            'fileUpload' => [
                'class' => FileUploadBehavior::className(),
                'image' => 'attachFile',
                'image_attribute' => 'file',
                'fileName' => 'rand',
                'deleteFile' => true,
                'path' => '/web/uploads/labs_students/',
                //'resize'=>['height'=>300],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['labs_id', 'student_id'], 'required'],
            [['description'], 'string'],
            [['attachFile'], 'file', 'extensions'=>'jpg, jpeg, png, doc, docx, xls, xlsx, ppt, pptx, rar, zip, txt, odt, pdf, djvu', 'skipOnEmpty'=>true],
            [['labs_id', 'student_id', 'points', 'create_at', 'update_at'], 'integer'],
            [['points'], 'validatePoints', 'on' => 'update'],
            [['file'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'labs_id' => 'Лабораторная работа',
            'student_id' => 'Студент',
            'description' => 'Описание',
            'file' => 'файл',
            'attachFile' => 'файл',
            'points' => 'Статус',
            'create_at' => 'Дата добавления',
            'update_at' => 'Дата редактирования',
        ];
    }

    public function validatePoints($attribute, $params)
    {
        $value = (int)$this->getAttribute($attribute);
        if ($value != 0 AND ($value < $this->labs->min_point OR $value > $this->labs->max_point)) {
            $this->addError($attribute, 'Если работа принимается, то она должна оцениваться минимум ' . $this->labs->min_point . ' баллами и максимум ' . $this->labs->max_point . ' баллами!');
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Students::className(), ['user_id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabs()
    {
        return $this->hasOne(Labs::className(), ['id' => 'labs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getLabsAvailableSender()
    {
        $labs = ArrayHelper::map(Labs::find()->with('labsStudents')->where('group_id = :group', [':group' => Yii::$app->user->identity->profile->group->id])->all(), 'id', 'title');
        //var_dump(Yii::$app->user->identity->profile);
        //var_dump($labs);die;
        $result = array();
        foreach($labs as $key => $value)
        {
            $labsStudentsId = LabsStudents::find()->where('labs_id = :lab AND student_id = :student', [':lab' => $key, ':student' => Yii::$app->user->id])->all();
            if(!empty($labsStudentsId))
                continue;
            else
                $result[$key] = $value;
        }
        return $result;
    }

    /** @inheritdoc */
    public function sendNotification()
    {
        Yii::$app->mailer->compose('labs/success', ['lab' => $this])
            ->setFrom('admin@kafedra.dev')
            ->setTo($this->student->user->email)
            ->setSubject('Ваша лабораторная работа принята')
            ->send();
    }
}

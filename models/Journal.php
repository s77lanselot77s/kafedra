<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "journal".
 *
 * @property integer $id
 * @property integer $tsg_id
 * @property string $description
 * @property integer $create_at
 * @property integer $update_at
 *
 * @property JournalRecords[] $journalRecords
 * @property TeacherSubjectGroups[] $tsg
 */
class Journal extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public $year;
    public $sem;
    public $planBranch;
    public $planSubject;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'journal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tsg_id'], 'required'],
            ['tsg_id', 'unique', 'message' => 'вы уже завели журнал на эту дисциплину'],
            [['tsg_id', 'create_at', 'update_at', 'year', 'sem', 'planBranch', 'planSubject'], 'integer'],
            [['description'], 'string']
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tsg_id' => 'Специальность / Дисциплина (по плану)',
            'description' => 'Описание',
            'year' => 'год',
            'sem' => 'семестр',
            'create_at' => 'Дата',
            'update_at' => 'Update At',
            'planBranch' => 'Специальность',
            'planSubject' => 'Предмет' ,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJournalRecords()
    {
        return $this->hasMany(JournalRecords::className(), ['journal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTsg()
    {
        return $this->hasOne(TeacherSubjectGroups::className(), ['id' => 'tsg_id']);
    }
}

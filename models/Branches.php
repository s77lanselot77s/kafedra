<?php
//специальность
namespace app\models;

use Yii;

/**
 * This is the model class for table "branches".
 *
 * @property integer $id
 * @property integer $number
 * @property string $title
 * @property string $description
 *
 * @property Groups[] $groups
 * @property SubjectBranches[] $subjectBranches
 */
class Branches extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'branches';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 256],
            [['number'], 'integer'],
            [['description'], 'string', 'max' => 2048],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'title' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['branch_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectBranches()
    {
        return $this->hasMany(SubjectBranches::className(), ['branch_id' => 'id']);
    }

    public function getNumTitle()
    {
        if($this->number)
            return $this->title . ' - (' . $this->number . ')';
        else
            return $this->title;
    }
}

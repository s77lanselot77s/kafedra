<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\behaviors\FileUploadBehavior;

/**
 * This is the model class for table "labs".
 *
 * @property integer $id
 * @property integer $tsg_id
 * @property integer $group_id
 * @property string $title
 * @property string $description
 * @property string $file
 * @property integer $min_point
 * @property integer $max_point
 * @property integer $create_at
 * @property integer $update_at
 *
 * @property Groups $group
 * @property SubjectBranches $tsg
 * @property LabsStudents[] $labsStudents
 */
class Labs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'labs';
    }

    /**
     * @inheritdoc
     */
    public $attachFile;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
            'fileUpload' => [
                'class' => FileUploadBehavior::className(),
                'image' => 'attachFile',
                'image_attribute' => 'file',
                'fileName' => 'rand',
                'deleteFile' => true,
                'path' => '/web/uploads/labs/',
                //'resize'=>['height'=>300],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tsg_id', 'title', 'group_id', 'description', 'min_point', 'max_point', 'create_at', 'update_at'], 'required'],
            [['attachFile'], 'file', 'extensions'=>'jpg, jpeg, png, doc, docx, xls, xlsx, ppt, pptx, rar, zip, txt, odt, pdf, djvu', 'skipOnEmpty'=>true],
            [['tsg_id', 'group_id', 'min_point', 'max_point'], 'integer'],
            [['description'], 'string'],
            [['title', 'file'], 'string', 'max' => 256],
            [['min_point', 'max_point'], 'required'],
            [['min_point', 'max_point'], 'integer', 'min'=>1, 'message' => 'Поле должно быть целым числом'],
            [
                ['min_point'],
                'compare',
                'compareAttribute'=>'max_point',
                'operator'=>'<=',
                'skipOnEmpty'=>true, 'message' => 'минимальное количество баллов должно быть меньше или равно максимального'
            ],
            [
                ['max_point'],
                'compare',
                'compareAttribute'=>'min_point',
                'operator'=>'>=', 'message' => 'максимальное количество баллов должно быть больше или равно минимальному'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tsg_id' => 'Специальность / Дисциплина (по плану)',
            'group_id' => 'группа',
            'title' => 'Название',
            'description' => 'Описание',
            'file' => 'файл',
            'attachFile' => 'файл',
            'min_point' => 'мин. баллов',
            'max_point' => 'макс. баллов',
            'create_at' => 'дата',
            'update_at' => 'Update At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTsg()
    {
        return $this->hasOne(TeacherSubjectGroups::className(), ['id' => 'tsg_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabsStudents()
    {
        return $this->hasMany(LabsStudents::className(), ['labs_id' => 'id']);
    }
}

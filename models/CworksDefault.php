<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cworks_default".
 *
 * @property integer $id
 * @property integer $tsg_id
 * @property string $ptitle
 * @property string $source_data
 * @property string $calculation_part
 * @property string $graphic_part
 * @property string $exper_part
 *
 * @property Cworks[] $cworks
 * @property SubjectBranches $tsg
 */
class CworksDefault extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cworks_default';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tsg_id', 'ptitle'], 'required'],
            [['id', 'tsg_id'], 'integer'],
            [['ptitle', 'source_data', 'calculation_part', 'graphic_part', 'exper_part'], 'string'],
            [['id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tsg_id' => 'Предмет/Преподаватель',
            'ptitle' => 'Название работы',
            'source_data' => 'Исходные данные',
            'calculation_part' => 'Расчетная чать',
            'graphic_part' => 'Графическая часть',
            'exper_part' => 'Экспериментальная часть',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCworks()
    {
        return $this->hasMany(Cworks::className(), ['cworks_default_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTsg()
    {
        return $this->hasOne(TeacherSubjectGroups::className(), ['id' => 'tsg_id']);
    }

}

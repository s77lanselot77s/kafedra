<?php

namespace app\models;

use app\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
/**
 * This is the model class for table "teachers".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $fname
 * @property string $lname
 * @property string $pname
 * @property string $degree
 * @property integer $create_at
 * @property string $update_at
 * @property string $fio
 * @property string $shortFio
 *
 * @property Groups[] $groups
 * @property TeacherSubjectGroups[] $teacherSubjectGroups
 * @property User $user
 */
class Teachers extends \yii\db\ActiveRecord
{
    //public $fio = '';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teachers';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fname', 'lname'], 'required'],
            [['user_id', 'create_at', 'update_at'], 'integer'],
            [['fname', 'lname', 'pname', 'degree'], 'string', 'max' => 64],
            [['fio'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID',
            'fname' => 'Имя',
            'lname' => 'Фамилия',
            'pname' => 'Отчество',
            'degree' => 'Ученая степень',
            'create_at' => 'Дата регистрации',
            'update_at' => 'Дата редактирования',
        ];
    }

    public function afterDelete()
    {
        $user = User::findOne(array('id'=>$this->user_id));
        if (isset($user))
            $user->delete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['curator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherSubjectGroups()
    {
        return $this->hasMany(TeacherSubjectGroups::className(), ['teacher_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getFio()
    {
        return $this->lname . ' ' . $this->fname . ' ' . $this->pname;
    }

    public function getShortFio()
    {
        return $this->lname . ' ' . mb_substr($this->fname,0,1,'UTF-8') . '. ' . mb_substr($this->pname,0,1,'UTF-8').'.';
    }
}

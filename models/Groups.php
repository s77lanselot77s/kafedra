<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "groups".
 *
 * @property integer $id
 * @property string $title
 * @property integer $branch_id
 * @property integer $curator_id
 *
 * @property Branches $branch
 * @property Teachers $curator
 * @property Students[] $students
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'branch_id', 'curator_id'], 'required'],
            [['id', 'branch_id', 'curator_id'], 'integer'],
            [['title'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование группы',
            'branch_id' => 'Специальность',
            'curator_id' => 'Куратор',
            'fio' => 'ФИО Куратора'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branches::className(), ['id' => 'branch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurator()
    {
        return $this->hasOne(Teachers::className(), ['user_id' => 'curator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Students::className(), ['group_id' => 'id']);
    }
}

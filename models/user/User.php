<?php

namespace app\models\user;

use app\models\Students;
use app\models\Teachers;
use dektrium\user\Finder;
use dektrium\user\helpers\Password;
use dektrium\user\Mailer;
use dektrium\user\Module;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\log\Logger;
use yii\web\IdentityInterface;

/**
 * User ActiveRecord model.
 *
 * Database fields:
 * @property integer $id
 * @property string  $username
 * @property string  $email
 * @property string  $unconfirmed_email
 * @property string  $password_hash
 * @property string  $auth_key
 * @property integer $registration_ip
 * @property integer $confirmed_at
 * @property integer $blocked_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 * @property boolean $student
 * @property boolean $teacher
 * Defined relations:
 * @property Account[] $accounts
 * @property Profile   $profile
 */

use dektrium\user\models\User as BaseUser;

class User extends BaseUser
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        if(\Yii::$app->user->can('student'))
            return $this->hasOne(Students::className(), ['user_id' => 'id']);
        else if (\Yii::$app->user->can('teacher') OR \Yii::$app->user->can('admin'))
            return $this->hasOne(Teachers::className(), ['user_id' => 'id']);
        else
            return false;
    }

    public function isStudent()
    {
        if(\Yii::$app->authManager->getRolesByUser($this->id) == 'student')
            return true;
        else
            return false;
    }

    public function isTeacher()
    {
        if(\Yii::$app->authManager->getRolesByUser($this->id) == 'teacher')
            return true;
        else
            return false;
    }


    /** @inheritdoc */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            // username rules
            ['username', 'required', 'on' => ['register', 'connect', 'create', 'update']],
            ['username', 'match', 'pattern' => '/^[a-zA-Z]\w+$/'],
            ['username', 'string', 'min' => 3, 'max' => 25],
            ['username', 'unique'],
            ['username', 'trim'],

            // email rules
            ['email', 'required', 'on' => ['register', 'connect', 'create', 'update']],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique'],
            ['email', 'trim'],

            // password rules
            ['password', 'required', 'on' => ['register']],
            ['password', 'string', 'min' => 6, 'on' => ['register', 'create']],
        ];
    }

    /** @inheritdoc */
    public function afterSave($insert, $changedAttributes)
    {
        /*if ($insert) {
            $profile = \Yii::createObject([
                'class'          => Profile::className(),
                'user_id'        => $this->id,
                'gravatar_email' => $this->email,
            ]);
            $profile->save(false);
        }*/
        parent::afterSave($insert, $changedAttributes);
    }
}
